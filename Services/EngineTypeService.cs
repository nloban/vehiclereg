﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;
using VehicleRegistrationExcelAddIn;

namespace Services
{
    public class EngineTypeService : IEngineTypeService
    {
        private readonly IEngineTypeRepository engineTypeRepository = new EngineTypeRepository();

        public void AddEngineType(String engineTypeName)
        {
            engineTypeRepository.Add(new EngineType
            {
                EngineTypeId = engineTypeName
            });
        }

        public List<EngineType> GetAllEngineTypes()
        {
            return engineTypeRepository.GetAll();
        }
    }
}
