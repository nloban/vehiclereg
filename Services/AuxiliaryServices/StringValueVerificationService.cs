﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services
{
    public class StringValueVerificationService
    {
        public static bool IsStringValueCorrect(String value)
        {
            return !String.IsNullOrWhiteSpace(value) && !value.StartsWith(" ") &&
                   !value.EndsWith(" ");
        }
    }
}
