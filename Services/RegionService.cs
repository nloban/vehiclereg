﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;
using VehicleRegistrationExcelAddIn;

namespace Services
{
    public class RegionService : IRegionService
    {
        private readonly IRegionRepository regionRepository = new RegionRepository();

        public void AddRegion(String regionName)
        {
            regionRepository.Add(new Region()
            {
                RegionId = regionName
            });
        }

        public List<Region> GetAllRegions()
        {
            return regionRepository.GetAll();
        }
    }
}
