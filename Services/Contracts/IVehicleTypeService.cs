﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VehicleRegistrationExcelAddIn;

namespace Services.Contracts
{
    public interface IVehicleTypeService
    {
        void AddVehicleType(String vehicleTypeName);

        List<VehicleType> GetAllVehicleTypes();
    }
}
