﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VehicleRegistrationExcelAddIn;

namespace Services.Contracts
{
    public interface IBrandService
    {
        void AddBrand(String brandName, String countryName);

        List<Brand> GetAllBrands();
    }
}
