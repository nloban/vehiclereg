﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VehicleRegistrationExcelAddIn;

namespace Services.Contracts
{
    public interface IDistrictService
    {
        void AddDistrict(String districtName, String regionName);

        List<District> GetDistrictsByRegion(String regionName);

        List<Region> GetRegionsByDistrict(String districtName);

        List<District> GetAllDistricts();
    }
}
