﻿using System;
using System.Collections.Generic;
using VehicleRegistrationExcelAddIn;

namespace Services.Contracts
{
    public interface IRegistrationFactService
    {
        void AddRegistrationFact(VehicleModel model, String color, String vehicleCategoryName,
            DateTime dateOfProduce, DateTime dateOfRegistration,
            String regionIdOfRegistration, String districtIdOfRegistration, String localityIdOfRegistration,
            String regionIdOfOwner, String districtIdOfOwner, String localityIdOfOwner);

        List<RegistrationFact> GetFactsByDates(DateTime startDate, DateTime endDate);

        Dictionary<Int32, List<RegistrationFact>> GetFactsByYears(Int32 startYear, Int32 endYear);

        Dictionary<String, List<RegistrationFact>> GetFactsByCarAgeAndRegions();
    }
}
