﻿using System;
using System.Collections.Generic;
using VehicleRegistrationExcelAddIn;

namespace Services.Contracts
{
    public interface IVehicleModelService
    {
        void AddVehicleModel(String nameOfModel, String brandName, String vehicleTypeName,
            Double engineSize, String engineTypeName, UInt32 weight, UInt32 maxWeight);

        List<VehicleModel> GetAllVehicleModels();
    }
}
