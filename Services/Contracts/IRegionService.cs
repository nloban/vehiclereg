﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VehicleRegistrationExcelAddIn;

namespace Services.Contracts
{
    public interface IRegionService
    {
        void AddRegion(String regionName);

        List<Region> GetAllRegions();
    }
}
