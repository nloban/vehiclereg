﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VehicleRegistrationExcelAddIn;

namespace Services.Contracts
{
    public interface IVehicleCategoryService
    {
        void AddVehicleCategory(String vehicleCategoryName);

        List<VehicleCategory> GetAllVehicleCategories();
    }
}
