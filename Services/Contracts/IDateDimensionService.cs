﻿using System;
using System.Collections.Generic;

namespace Services.Contracts
{
    public interface IDateDimensionService
    {
        DateTime GetMinRegistrationDate();

        DateTime GetMaxRegistrationDate();

        Dictionary<Int32, Int32> GetAmountOfRegistered(Int32 year);
    }
}
