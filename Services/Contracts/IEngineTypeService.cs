﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VehicleRegistrationExcelAddIn;

namespace Services.Contracts
{
    public interface IEngineTypeService
    {
        void AddEngineType(String engineTypeName);

        List<EngineType> GetAllEngineTypes();
    }
}
