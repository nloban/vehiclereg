﻿using System;
using System.Collections.Generic;
using VehicleRegistrationExcelAddIn;

namespace Services.Contracts
{
    public interface ILocalityService
    {
        void AddLocality(String localityName, String districtName, String regionName);

        List<Locality> GetAllLocalities();

        List<District> GetDistrictsByLocality(String localityName);

        List<Locality> GetLocalitiesByDistrict(String districtName);

        List<Locality> GetLocalitiesByRegion(String regionName);
    }
}
