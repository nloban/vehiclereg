﻿using System;
using System.Collections.Generic;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;
using VehicleRegistrationExcelAddIn;

namespace Services
{
    public class VehicleCategoryService : IVehicleCategoryService
    {
        private readonly IVehicleCategoryRepository vehicleCategoryRepository = new VehicleCategoryRepository();

        public void AddVehicleCategory(String vehicleCategoryName)
        {
            vehicleCategoryRepository.Add(new VehicleCategory()
            {
                VehicleCategoryId = vehicleCategoryName
            });
        }

        public List<VehicleCategory> GetAllVehicleCategories()
        {
            return vehicleCategoryRepository.GetAll();
        }
    }
}
