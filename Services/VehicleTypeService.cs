﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;
using VehicleRegistrationExcelAddIn;

namespace Services
{
    public class VehicleTypeService : IVehicleTypeService
    {
        private readonly IVehicleTypeRepository vehicleTypeRepository = new VehicleTypeRepository();

        public void AddVehicleType(String vehicleTypeName)
        {
            vehicleTypeRepository.Add(new VehicleType()
            {
                VehicleTypeId = vehicleTypeName
            });
        }

        public List<VehicleType> GetAllVehicleTypes()
        {
            return vehicleTypeRepository.GetAll();
        } 
    }
}
