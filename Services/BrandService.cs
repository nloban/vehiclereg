﻿using System;
using System.Collections.Generic;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;
using VehicleRegistrationExcelAddIn;

namespace Services
{
    public class BrandService : IBrandService
    {
        private readonly IBrandRepository brandRepository = new BrandRepository();

        public void AddBrand(String brandName, String countryName)
        {
            brandRepository.Add(new Brand
            {
                BrandId = brandName,
                CountryId = countryName
            });
        }

        public List<Brand> GetAllBrands()
        {
            return brandRepository.GetAll();
        } 
    }
}
