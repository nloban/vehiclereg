﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;
using VehicleRegistrationExcelAddIn;

namespace Services
{
    public class VehicleModelService : IVehicleModelService
    {
        private readonly IVehicleModelRepository vehicleModelRepository = new VehicleModelRepository();

        public void AddVehicleModel(String nameOfModel, String brandName, String vehicleTypeName, 
            Double engineSize, String engineTypeName, UInt32 weight, UInt32 maxWeight)
        {
            vehicleModelRepository.Add(new VehicleModel
            {
                NameOfModel = nameOfModel,
                BrandId = brandName,
                VehicleTypeId = vehicleTypeName,
                EngineSize = engineSize,
                EngineTypeId = engineTypeName,
                Weight = weight,
                MaxPermittedWeight = maxWeight
            });
        }

        public List<VehicleModel> GetAllVehicleModels()
        {
            return vehicleModelRepository.GetAll().OrderBy(model => model.BrandId)
                .ThenBy(model => model.NameOfModel)
                .ThenBy(model => model.VehicleTypeId)
                .ThenBy(model => model.EngineTypeId)
                .ThenBy(model => model.EngineSize).ToList();
        }
    }
}
