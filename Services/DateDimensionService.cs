﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;

namespace Services
{
    public class DateDimensionService : IDateDimensionService
    {
        private readonly IDateDimensionRepository dateDimensionRepository = new DateDimensionRepository();

        private readonly IRegistrationFactRepository registrationFactRepository = new RegistrationFactRepository();

        public DateTime GetMinRegistrationDate()
        {
            return dateDimensionRepository.GetAll().Min(date => date.DateId);
        }

        public DateTime GetMaxRegistrationDate()
        {
            return dateDimensionRepository.GetAll().Max(date => date.DateId);
        }

        public Dictionary<Int32, Int32> GetAmountOfRegistered(Int32 year)
        {
            var dictionary = new Dictionary<Int32, Int32>();
            var facts = registrationFactRepository.GetBy(fact => fact.DateOfRegistrationId.Year == year);
            for (int i = 1; i <= 12; i++)
            {
                dictionary.Add(i, facts.Where(fact => fact.DateOfRegistrationId.Month == i).Count());
            }
            return dictionary;
        }
    }
}
