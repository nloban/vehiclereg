﻿using System;
using System.Collections.Generic;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;
using VehicleRegistrationExcelAddIn;

namespace Services
{
    public class ColorService : IColorService
    {
        private readonly IColorRepository colorRepository = new ColorRepository();

        public void AddColor(String colorName)
        {
            colorRepository.Add(new Color()
            {
                ColorName = colorName
            });
        }

        public List<Color> GetAllColors()
        {
            return colorRepository.GetAll();
        } 
    }
}
