﻿using System;
using System.IO;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;
using VehicleRegistrationExcelAddIn;

namespace Services
{
    public class DbCreationService : IDbCreationService
    {
        private readonly VehicleRegistrationModel vehicleRegistrationModel = VehicleRegistrationModel.Current;
        private readonly IColorRepository colorRepository = new ColorRepository();
        private readonly IEngineTypeRepository engineTypeRepository = new EngineTypeRepository();
        private readonly ICountryOfProductionRepository countryOfProductionRepository = new CountryOfProductionRepository();
        private readonly IBrandRepository brandRepository = new BrandRepository();
        private readonly IVehicleModelRepository vehicleModelRepository = new VehicleModelRepository();
        private readonly IVehicleTypeRepository vehicleTypeRepository = new VehicleTypeRepository();
        private readonly IVehicleCategoryRepository vehicleCategoryRepository = new VehicleCategoryRepository();
        private readonly IRegionRepository regionRepository = new RegionRepository();
        private readonly IDistrictRepository districtRepository = new DistrictRepository();
        private readonly ILocalityRepository localityRepository = new LocalityRepository();

        private CountryOfProduction[] countryOfProductions;
        private Brand[] brands;
        private VehicleModel[] models;
        private EngineType[] engineTypes;
        private VehicleType[] vehicleTypes;
        private VehicleCategory[] vehicleCategories;
        private Region[] regions;
        private District[] districts;
        private Locality[] localities;

        public DbCreationService()
        {
            CreateDatabase();
        }

        private void CreateDatabase()
        {
            if (!vehicleRegistrationModel.GetSchemaHandler().DatabaseExists())
            {
                vehicleRegistrationModel.GetSchemaHandler().CreateDatabase();

                string theFilePath = String.Format(@"{0}\{1}", AppDomain.CurrentDomain.BaseDirectory, "VehicleRegistrationConnection.rlinq.sql");

                using (var streamReader = new StreamReader(theFilePath))
                {
                    vehicleRegistrationModel.GetSchemaHandler().ExecuteDDLScript(streamReader.ReadToEnd());
                }

                GenerateDefaultData();
            }            
        }

        private void GenerateDefaultData()
        {
            GenerateDefaultColors();
            GenerateDefaultEngineTypes();
            GenerateDefaultCountryOfProductions();
            GenerateDefaultBrands();
            GenerateDefaultVehicleTypes();
            GenerateDefaultVehicleCategories();
            GenerateDefaultModels();
            GenerateDefaultRegions();
            GenerateDefaultDistricts();
            GenerateDefaultLicalities();
        }

        #region Default measures generation
        private void GenerateDefaultColors()
        {
            Color[] colors =
            {
                new Color { ColorName = "Белый" }, 
                new Color { ColorName = "Красный" }, 
                new Color { ColorName = "Синий" },
                new Color { ColorName = "Зеленый" },
                new Color { ColorName = "Желтый" },
                new Color { ColorName = "Черный" }
            };

            foreach (var color in colors)
            {
                colorRepository.Add(color);
            }
        }

        private void GenerateDefaultVehicleTypes()
        {
            vehicleTypes = new[]
            {
                new VehicleType {VehicleTypeId = "Универсал"},
                new VehicleType {VehicleTypeId = "Седан"},
                new VehicleType {VehicleTypeId = "Хетчбэк"},
                new VehicleType {VehicleTypeId = "Минивен"},
                new VehicleType {VehicleTypeId = "Внедорожник"},
                new VehicleType {VehicleTypeId = "Кабриолет"},
                new VehicleType {VehicleTypeId = "Микроавтобуc"},
                new VehicleType {VehicleTypeId = "Фургон"}

            };

            foreach (var vehicleType in vehicleTypes)
            {
                vehicleTypeRepository.Add(vehicleType);
            }
        }

        private void GenerateDefaultEngineTypes()
        {
            engineTypes = new []
            {
                new EngineType { EngineTypeId = "Бензин" }, 
                new EngineType { EngineTypeId = "Дизель" }, 
                new EngineType { EngineTypeId = "Газ (бензин)" }, 
                new EngineType { EngineTypeId = "Гибрид (бензин)" }, 
                new EngineType { EngineTypeId = "Гибрид (дизель)" }, 
                new EngineType { EngineTypeId = "Электромобиль" }
            };

            foreach (var engineType in engineTypes)
            {
                engineTypeRepository.Add(engineType);
            }
        }

        private void GenerateDefaultCountryOfProductions()
        {
            countryOfProductions = new CountryOfProduction[]
            {
                new CountryOfProduction { CountryId = "Германия" },
                new CountryOfProduction { CountryId = "Франция" },
                new CountryOfProduction { CountryId = "Италия" },
                new CountryOfProduction { CountryId = "Россия" },
                new CountryOfProduction { CountryId = "Великобритания" },
                new CountryOfProduction { CountryId = "Япония" },
                new CountryOfProduction { CountryId = "США" },
                new CountryOfProduction { CountryId = "Китай" },
                new CountryOfProduction { CountryId = "Южная Корея" },
                new CountryOfProduction { CountryId = "Испания" },
                new CountryOfProduction { CountryId = "Чехия" },
                new CountryOfProduction { CountryId = "Швеция" }
            };

            foreach (var countryOfProduction in countryOfProductions)
            {
                countryOfProductionRepository.Add(countryOfProduction);
            }
        }

        private void GenerateDefaultBrands()
        {
            brands = new[]
            {
                new Brand {BrandId = "Volkswagen", CountryId = "Германия"},
                new Brand {BrandId = "Opel", CountryId = "Германия"},
                new Brand {BrandId = "Audi", CountryId = "Германия"},
                new Brand {BrandId = "Ford", CountryId = "США"},
                new Brand {BrandId = "Renault", CountryId = "Франция"},
                new Brand {BrandId = "Peugeot", CountryId = "Франция"},
                new Brand {BrandId = "Mercedes-Benz", CountryId = "Германия"},
                new Brand {BrandId = "Citroen", CountryId = "Франция"},
                new Brand {BrandId = "BMW", CountryId = "Германия"},
                new Brand {BrandId = "Mazda", CountryId = "Япония"},
                new Brand {BrandId = "Toyota", CountryId = "Япония"},
                new Brand {BrandId = "Nissan", CountryId = "Япония"},
                new Brand {BrandId = "Fiat", CountryId = "Италия"},
                new Brand {BrandId = "Mitsubishi", CountryId = "Япония"},
                new Brand {BrandId = "Hyundai", CountryId = "Южная Корея"},
                new Brand {BrandId = "Volvo", CountryId = "Швеция"},
                new Brand {BrandId = "Honda", CountryId = "Япония"},
                new Brand {BrandId = "SEAT", CountryId = "Испания"},
                new Brand {BrandId = "Kia", CountryId = "Южная Корея"},
                new Brand {BrandId = "Skoda", CountryId = "Чехия"},

                new Brand {BrandId = "Hymer", CountryId = "Германия"},
                new Brand {BrandId = "MAN", CountryId = "Германия"},
                new Brand {BrandId = "Maybach", CountryId = "Германия"},
                new Brand {BrandId = "Porsche", CountryId = "Германия"},

                new Brand {BrandId = "Lexus", CountryId = "Япония"},
                new Brand {BrandId = "Infiniti", CountryId = "Япония"},
                new Brand {BrandId = "Subaru", CountryId = "Япония"},
                
                new Brand {BrandId = "Daewoo", CountryId = "Южная Корея"},
                
                new Brand {BrandId = "Chevrolet", CountryId = "США"},
                new Brand {BrandId = "Chrysler", CountryId = "США"},
                new Brand {BrandId = "Eagle", CountryId = "США"},
                new Brand {BrandId = "Hummer", CountryId = "США"},
                new Brand {BrandId = "Jeep", CountryId = "США"},
                new Brand {BrandId = "Lincoln", CountryId = "США"},
                new Brand {BrandId = "Mercury", CountryId = "США"},
                new Brand {BrandId = "Plymouth", CountryId = "США"},
                new Brand {BrandId = "Pontiac", CountryId = "США"},
                new Brand {BrandId = "Saturn", CountryId = "США"},


                new Brand {BrandId = "Chery", CountryId = "Китай"},
                new Brand {BrandId = "FAW", CountryId = "Китай"},
                new Brand {BrandId = "Great Wall", CountryId = "Китай"},

                new Brand {BrandId = "AC", CountryId = "Великобритания "},
                new Brand {BrandId = "Aston Martin", CountryId = "Великобритания "},
                new Brand {BrandId = "Bentley", CountryId = "Великобритания "},
                new Brand {BrandId = "Bristol", CountryId = "Великобритания "},
                new Brand {BrandId = "Daimler", CountryId = "Великобритания "},
                new Brand {BrandId = "Jaguar", CountryId = "Великобритания "},
                new Brand {BrandId = "Land Rover", CountryId = "Великобритания "},
                new Brand {BrandId = "LDV", CountryId = "Великобритания "},
                new Brand {BrandId = "Lotus", CountryId = "Великобритания "},
                new Brand {BrandId = "Metrocab", CountryId = "Великобритания "},
                new Brand {BrandId = "MINI", CountryId = "Великобритания "},
                new Brand {BrandId = "Morgan", CountryId = "Великобритания "},
                new Brand {BrandId = "Rolls-Royce", CountryId = "Великобритания "},
                new Brand {BrandId = "Rover", CountryId = "Великобритания "},
            };

            foreach (var brand in brands)
            {
                brandRepository.Add(brand);
            }
        }

        private void GenerateDefaultModels()
        {
            models = new VehicleModel[]
            {
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Beetle",
                    EngineTypeId = "Бензин",
                    EngineSize = 1.8,
                    VehicleTypeId = "Хетчбэк",
                    Weight = 1290,
                    MaxPermittedWeight = 1680
                },                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Beetle",
                    EngineTypeId = "Гибрид (бензин)",
                    EngineSize = 1.8,
                    VehicleTypeId = "Хетчбэк",
                    Weight = 1290,
                    MaxPermittedWeight = 1680
                },                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Beetle",
                    EngineTypeId = "Гибрид (дизель)",
                    EngineSize = 1.8,
                    VehicleTypeId = "Хетчбэк",
                    Weight = 1290,
                    MaxPermittedWeight = 1680
                },                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Beetle",
                    EngineTypeId = "Электромобиль",
                    EngineSize = 1.8,
                    VehicleTypeId = "Хетчбэк",
                    Weight = 1290,
                    MaxPermittedWeight = 1680
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Beetle",
                    EngineTypeId = "Дизель",
                    EngineSize = 1.9,
                    VehicleTypeId = "Хетчбэк",
                    Weight = 1291,
                    MaxPermittedWeight = 1660
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Bora",
                    EngineTypeId = "Бензин",
                    EngineSize = 2,
                    VehicleTypeId = "Седан",
                    Weight = 1265,
                    MaxPermittedWeight = 1820
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Bora",
                    EngineTypeId = "Бензин",
                    EngineSize = 2.3,
                    VehicleTypeId = "Седан",
                    Weight = 1265,
                    MaxPermittedWeight = 1820
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Bora",
                    EngineTypeId = "Газ (бензин)",
                    EngineSize = 1.6,
                    VehicleTypeId = "Универсал",
                    Weight = 1290,
                    MaxPermittedWeight = 1840
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Bora",
                    EngineTypeId = "Дизель",
                    EngineSize = 1.9,
                    VehicleTypeId = "Седан",
                    Weight = 1265,
                    MaxPermittedWeight = 1820
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Bora",
                    EngineTypeId = "Дизель",
                    EngineSize = 1.9,
                    VehicleTypeId = "Универсал",
                    Weight = 1290,
                    MaxPermittedWeight = 1840
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Caddy",
                    EngineTypeId = "Газ (бензин)",
                    EngineSize = 1.9,
                    VehicleTypeId = "Фургон",
                    Weight = 1520,
                    MaxPermittedWeight = 2010
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Caddy",
                    EngineTypeId = "Дизель",
                    EngineSize = 2,
                    VehicleTypeId = "Универсал",
                    Weight = 1480,
                    MaxPermittedWeight = 1960
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Caddy",
                    EngineTypeId = "Дизель",
                    EngineSize = 1.9,
                    VehicleTypeId = "Седан",
                    Weight = 1520,
                    MaxPermittedWeight = 2010
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Caddy",
                    EngineTypeId = "Дизель",
                    EngineSize = 2,
                    VehicleTypeId = "Минивен",
                    Weight = 1480,
                    MaxPermittedWeight = 1960
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Caddy",
                    EngineTypeId = "Дизель",
                    EngineSize = 2,
                    VehicleTypeId = "Хетчбэк",
                    Weight = 1480,
                    MaxPermittedWeight = 1960
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Caddy",
                    EngineTypeId = "Газ (бензин)",
                    EngineSize = 1.9,
                    VehicleTypeId = "Фургон",
                    Weight = 1520,
                    MaxPermittedWeight = 2010
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Caddy",
                    EngineTypeId = "Бензин",
                    EngineSize = 2,
                    VehicleTypeId = "Универсал",
                    Weight = 1480,
                    MaxPermittedWeight = 1960
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Caddy",
                    EngineTypeId = "Бензин",
                    EngineSize = 1.9,
                    VehicleTypeId = "Седан",
                    Weight = 1520,
                    MaxPermittedWeight = 2010
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Caddy",
                    EngineTypeId = "Бензин",
                    EngineSize = 2,
                    VehicleTypeId = "Минивен",
                    Weight = 1480,
                    MaxPermittedWeight = 1960
                },
                new VehicleModel
                {
                    BrandId = "Volkswagen",
                    NameOfModel = "Caddy",
                    EngineTypeId = "Бензин",
                    EngineSize = 2,
                    VehicleTypeId = "Хетчбэк",
                    Weight = 1480,
                    MaxPermittedWeight = 1960
                }
//                new VehicleModel {BrandId = "Volkswagen", NameOfModel = "Golf", EngineTypeId = "", VehicleTypeId = "", Weight = "", MaxPermittedWeight = ""},
//                new VehicleModel {BrandId = "Volkswagen", NameOfModel = "Jetta", EngineTypeId = "", VehicleTypeId = "", Weight = "", MaxPermittedWeight = ""},
//                new VehicleModel {BrandId = "Volkswagen", NameOfModel = "Passat", EngineTypeId = "", VehicleTypeId = "", Weight = "", MaxPermittedWeight = ""},
//                new VehicleModel {BrandId = "Volkswagen", NameOfModel = "Polo", EngineTypeId = "", VehicleTypeId = "", Weight = "", MaxPermittedWeight = ""},
//                new VehicleModel {BrandId = "Volkswagen", NameOfModel = "Santana", EngineTypeId = "", VehicleTypeId = "", Weight = "", MaxPermittedWeight = ""},
//                new VehicleModel {BrandId = "Volkswagen", NameOfModel = "Sharan", EngineTypeId = "", VehicleTypeId = "", Weight = "", MaxPermittedWeight = ""},
//                new VehicleModel {BrandId = "Volkswagen", NameOfModel = "Tiguan", EngineTypeId = "", VehicleTypeId = "", Weight = "", MaxPermittedWeight = ""},
//                new VehicleModel {BrandId = "Volkswagen", NameOfModel = "Touran", EngineTypeId = "", VehicleTypeId = "", Weight = "", MaxPermittedWeight = ""},
//                new VehicleModel {BrandId = "Volkswagen", NameOfModel = "Touareg", EngineTypeId = "", VehicleTypeId = "", Weight = "", MaxPermittedWeight = ""},
            };

            foreach (var model in models)
            {
                vehicleModelRepository.Add(model);
            }
        }

        private void GenerateDefaultVehicleCategories()
        {
            vehicleCategories = new[]
            {
                new VehicleCategory {VehicleCategoryId = "B"}
            };

            foreach (var vehicleCategory in vehicleCategories)
            {
                vehicleCategoryRepository.Add(vehicleCategory);
            }
        }

        private void GenerateDefaultRegions()
        {
            regions = new[]
            {
                new Region {RegionId = "Минская"},
                new Region {RegionId = "Витебская"},
                new Region {RegionId = "Гомельская"},
                new Region {RegionId = "Брестская"},
                new Region {RegionId = "Могилевская"},
                new Region {RegionId = "Гродненская"}
            };

            foreach (var region in regions)
            {
                regionRepository.Add(region);
            }
        }

        private void GenerateDefaultDistricts()
        {
            districts = new[]
            {
                new District {DistrictId = "Минский", RegionId = "Минская"},
                new District {DistrictId = "Вилейский", RegionId = "Минская"},
                new District {DistrictId = "Слуцкий", RegionId = "Минская"},
                new District {DistrictId = "Молодечненский", RegionId = "Минская"},
                new District {DistrictId = "Борисовский", RegionId = "Минская"},
                new District {DistrictId = "Логойский", RegionId = "Минская"},
                new District {DistrictId = "Полоцкий", RegionId = "Витебская"},
                new District {DistrictId = "Брасловский", RegionId = "Витебская"},
                new District {DistrictId = "Поставский", RegionId = "Витебская"},
                new District {DistrictId = "Глубокский", RegionId = "Витебская"},
                new District {DistrictId = "Речицкий", RegionId = "Гомельская"},
                new District {DistrictId = "Жлобинский", RegionId = "Гомельская"},
                new District {DistrictId = "Мозырский", RegionId = "Гомельская"},
                new District {DistrictId = "Калинковичский", RegionId = "Гомельская"},
                new District {DistrictId = "Лельчицкий", RegionId = "Гомельская"},
                new District {DistrictId = "Пинский", RegionId = "Брестская"},
                new District {DistrictId = "Лунинецкий", RegionId = "Брестская"},
                new District {DistrictId = "Барановичский", RegionId = "Брестская"},
                new District {DistrictId = "Ивановский", RegionId = "Брестская"},
                new District {DistrictId = "Березовский", RegionId = "Брестская"},
                new District {DistrictId = "Шкловский", RegionId = "Могилевская"},
                new District {DistrictId = "Климовичский", RegionId = "Могилевская"},
                new District {DistrictId = "Могилевский", RegionId = "Могилевская"},
                new District {DistrictId = "Осиповичский", RegionId = "Могилевская"},
                new District {DistrictId = "Бобруйский", RegionId = "Могилевская"},
                new District {DistrictId = "Лидский", RegionId = "Гродненская"},
                new District {DistrictId = "Сморгонский", RegionId = "Гродненская"},
                new District {DistrictId = "Новогрудский", RegionId = "Гродненская"},
                new District {DistrictId = "Слонимский", RegionId = "Гродненская"},
            };

            foreach (var district in districts)
            {
                districtRepository.Add(district);
            }
        }

        private void GenerateDefaultLicalities()
        {
            localities = new[]
            {
                new Locality {LocalityId = "Минск", DistrictId = "Минский", RegionId = "Минская"},
                new Locality {LocalityId = "Вилейка", DistrictId = "Вилейский", RegionId = "Минская"},
                new Locality {LocalityId = "Слуцк", DistrictId = "Слуцкий", RegionId = "Минская"},
                new Locality {LocalityId = "Молодечно", DistrictId = "Молодечненский", RegionId = "Минская"},
                new Locality {LocalityId = "Борисов", DistrictId = "Борисовский", RegionId = "Минская"},
                new Locality {LocalityId = "Логойск", DistrictId = "Логойский", RegionId = "Минская"},
                new Locality {LocalityId = "Полоцк", DistrictId = "Полоцкий", RegionId = "Витебская"},
                new Locality {LocalityId = "Браслов", DistrictId = "Брасловский", RegionId = "Витебская"},
                new Locality {LocalityId = "Поставы", DistrictId = "Поставский", RegionId = "Витебская"},
                new Locality {LocalityId = "Глубокое", DistrictId = "Глубокский", RegionId = "Витебская"},
                new Locality {LocalityId = "Речица", DistrictId = "Речицкий", RegionId = "Гомельская"},
                new Locality {LocalityId = "Жлобин", DistrictId = "Жлобинский", RegionId = "Гомельская"},
                new Locality {LocalityId = "Мозырь", DistrictId = "Мозырский", RegionId = "Гомельская"},
                new Locality {LocalityId = "Калинковичи", DistrictId = "Калинковичский", RegionId = "Гомельская"},
                new Locality {LocalityId = "Лельчицы", DistrictId = "Лельчицкий", RegionId = "Гомельская"},
                new Locality {LocalityId = "Пинск", DistrictId = "Пинский", RegionId = "Брестская"},
                new Locality {LocalityId = "Лунинец", DistrictId = "Лунинецкий", RegionId = "Брестская"},
                new Locality {LocalityId = "Барановичи", DistrictId = "Барановичский", RegionId = "Брестская"},
                new Locality {LocalityId = "Иваново", DistrictId = "Ивановский", RegionId = "Брестская"},
                new Locality {LocalityId = "Береза", DistrictId = "Березовский", RegionId = "Брестская"},
                new Locality {LocalityId = "Шклов", DistrictId = "Шкловский", RegionId = "Могилевская"},
                new Locality {LocalityId = "Климовичи", DistrictId = "Климовичский", RegionId = "Могилевская"},
                new Locality {LocalityId = "Могилев", DistrictId = "Могилевский", RegionId = "Могилевская"},
                new Locality {LocalityId = "Осиповичи", DistrictId = "Осиповичский", RegionId = "Могилевская"},
                new Locality {LocalityId = "Бобруйск", DistrictId = "Бобруйский", RegionId = "Могилевская"},
                new Locality {LocalityId = "Лида", DistrictId = "Лидский", RegionId = "Гродненская"},
                new Locality {LocalityId = "Сморгонь", DistrictId = "Сморгонский", RegionId = "Гродненская"},
                new Locality {LocalityId = "Новогрудок", DistrictId = "Новогрудский", RegionId = "Гродненская"},
                new Locality {LocalityId = "Слоним", DistrictId = "Слонимский", RegionId = "Гродненская"},
            };

            foreach (var locality in localities)
            {
                localityRepository.Add(locality);
            }
        }

        #endregion
    }
}
