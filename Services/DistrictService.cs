﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;
using VehicleRegistrationExcelAddIn;

namespace Services
{
    public class DistrictService : IDistrictService
    {
        private readonly IDistrictRepository districtRepository = new DistrictRepository();

        public void AddDistrict(String districtName, String regionName)
        {
            districtRepository.Add(new District()
            {
                DistrictId = districtName,
                RegionId = regionName
            });
        }

        public List<District> GetAllDistricts()
        {
            return districtRepository.GetAll();
        }

        public List<District> GetDistrictsByRegion(String regionName)
        {
            return districtRepository.GetBy(disctrict => disctrict.RegionId.Equals(regionName));
        }

        public List<Region> GetRegionsByDistrict(String districtName)
        {
            return districtRepository.GetBy(disctrict => disctrict.DistrictId.Equals(districtName)).Select(d => d.Region).ToList();
        }
    }
}
