﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;
using Telerik.OpenAccess.Exceptions;
using VehicleRegistrationExcelAddIn;

namespace Services
{
    public class RegistrationFactService : IRegistrationFactService
    {
        private readonly IRegistrationFactRepository registrationFactRepository = new RegistrationFactRepository();
        private readonly IDateDimensionRepository dateDimensionRepository = new DateDimensionRepository();
        private readonly IRegionRepository regionRepository = new RegionRepository();

        public void AddRegistrationFact(VehicleModel model, String color, String vehicleCategoryName, 
            DateTime dateOfProduce, DateTime dateOfRegistration,
            String regionIdOfRegistration, String districtIdOfRegistration, String localityIdOfRegistration,
            String regionIdOfOwner, String districtIdOfOwner, String localityIdOfOwner)
        {

            AddDateDimensionToDatabase(dateOfProduce.Date);
            AddDateDimensionToDatabase(dateOfRegistration.Date);

            try
            {
                registrationFactRepository.Add(new RegistrationFact
                {
                    ModelId = model.ModelId,
                    VehicleCategoryId = vehicleCategoryName,
                    Color = color,
                    DateOfProduceId = dateOfProduce.Date,
                    DateOfRegistrationId = dateOfRegistration.Date,
                    RegionIdOfOwner = regionIdOfOwner,
                    DistrictIdOfOwner = districtIdOfOwner,
                    LocalityIdOfOwner = localityIdOfOwner,
                    RegionIdOfRegistration = regionIdOfRegistration,
                    DistrictIdOfRegistration = districtIdOfRegistration,
                    LocalityIdOfRegistration = localityIdOfRegistration
                });
            }
            catch (Exception)
            {
            }
        }

        public List<RegistrationFact> GetFactsByDates(DateTime startDate, DateTime endDate)
        {
            return
                registrationFactRepository.GetBy(
                    fact => fact.DateOfRegistrationId >= startDate && fact.DateOfRegistrationId <= endDate).OrderBy(fact => fact.DateOfRegistrationId).ToList();
        }

        public Dictionary<Int32, List<RegistrationFact>> GetFactsByYears(Int32 startYear, Int32 endYear)
        {
            var dictionary = new Dictionary<Int32, List<RegistrationFact>>();
            for (int i = startYear; i <= endYear; i++)
            {
                dictionary.Add(i, registrationFactRepository.GetBy(fact => fact.DateOfRegistrationId.Year == i));
            }
            return dictionary;
        }

        public Dictionary<String, List<RegistrationFact>> GetFactsByCarAgeAndRegions()
        {
            var regions = regionRepository.GetAll();
            var dictionary = new Dictionary<String, List<RegistrationFact>>();
            foreach (var region in regions)
            {
                dictionary.Add(region.RegionId, registrationFactRepository.GetBy(fact => fact.RegionIdOfOwner.Equals(region.RegionId)));
            }
            return dictionary;
        }

        private void AddDateDimensionToDatabase(DateTime dateTime)
        {
            try
            {
                dateDimensionRepository.Add(new DateDimension
                {
                    DateId = dateTime
                });
            }
            catch (DuplicateKeyException exception)
            {
            }
        }
    }
}
