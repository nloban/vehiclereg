﻿using System.Collections.Generic;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;
using VehicleRegistrationExcelAddIn;

namespace Services
{
    public class CountryOfProductionService : ICountryOfProductionService
    {
        private readonly ICountryOfProductionRepository countryOfProductionRepository = new CountryOfProductionRepository();

        public List<CountryOfProduction> GetAllCountryOfProductions()
        {
            return countryOfProductionRepository.GetAll();
        } 
    }
}
