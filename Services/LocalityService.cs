﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Services.Contracts;
using VehicleRegistrationExcelAddIn;

namespace Services
{
    public class LocalityService : ILocalityService
    {
        private readonly ILocalityRepository localityRepository = new LocalityRepository();

        public void AddLocality(String localityName, String districtName, String regionName)
        {
            localityRepository.Add(new Locality()
            {
                LocalityId = localityName,
                DistrictId = districtName,
                RegionId = regionName
            });
        }

        public List<Locality> GetAllLocalities()
        {
            return localityRepository.GetAll();
        }

        public List<District> GetDistrictsByLocality(String localityName)
        {
            return localityRepository.GetBy(locality => locality.LocalityId.Equals(localityName)).Select(l => l.District).ToList();
        }

        public List<Locality> GetLocalitiesByDistrict(String districtName)
        {
            return localityRepository.GetBy(locality => locality.DistrictId.Equals(districtName)).ToList();
        }

        public List<Locality> GetLocalitiesByRegion(String regionName)
        {
            return localityRepository.GetBy(locality => locality.RegionId.Equals(regionName)).ToList();
        }
    }
}
