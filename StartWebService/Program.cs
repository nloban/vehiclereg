﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using VehicleRegistrationWcfServiceLibrary;

namespace StartWebService
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(ExecuteQueryService));

            host.Open();

            Console.WriteLine("Service is up and running");

            Console.ReadLine();

            host.Close();
        }
    }
}
