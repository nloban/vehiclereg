﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace VehicleRegistrationWcfServiceLibrary
{
    [ServiceContract]
    public interface IExecuteQueryService
    {
        [OperationContract]
        String ExecuteSelectStatement(String query);

        [OperationContract]
        void GenerateFacts(DateTime startDate, DateTime endDate, UInt32 amount);

    }
}
