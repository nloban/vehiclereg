﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using VehicleRegistrationExcelAddIn;

namespace VehicleRegistrationWcfServiceLibrary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ExecuteQueryService" in both code and config file together.
    public class ExecuteQueryService : IExecuteQueryService
    {
        private readonly IDateDimensionRepository dateDimensionRepository = new DateDimensionRepository();
        private readonly IRegistrationFactRepository registrationFactRepository = new RegistrationFactRepository();

        public String ExecuteSelectStatement(String query)
        {
            String result = String.Empty;
            if (query.Split(' ')[0].ToUpper() == "SELECT")
            {
                try
                {
                    result = "";
                    using (SqlConnection connection =
                        new SqlConnection((ConfigurationManager.ConnectionStrings["VehicleRegistrationConnection"]).ConnectionString)
                        )
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(query, connection);
                        command.CommandType = CommandType.Text;
                        command.CommandTimeout = 0;
                        using (IDataReader rdr = command.ExecuteReader())
                        {
                            for (int i = 0; i < rdr.FieldCount; i++)
                            {
                                result += rdr.GetName(i) + "|";
                            }
                            result += "\n";
                            while (rdr.Read())
                            {
                                for (int i = 0; i < rdr.FieldCount; i++)
                                {
                                    result += rdr[i] + "|";
                                }
                                result += "\n";
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    result = "Проверьте правильность введенного запроса!";
                }
            }
            else
            {
                result = "Введеннное выражение не является SELECT запросом!";
            }

            return result;
        }

        public void GenerateFacts(DateTime startDate, DateTime endDate, UInt32 amount)
        {
            Random random = new Random();
            var localities = new HashSet<Locality>(VehicleRegistrationModel.Current.Localities);
            var localitiesCount = localities.Count;
            var colors = new HashSet<Color>(VehicleRegistrationModel.Current.Colors);
            var colorsCount = colors.Count;
            var models = new HashSet<VehicleModel>(VehicleRegistrationModel.Current.VehicleModels);
            var modelsCount = models.Count;
            var vehicleCategories = new HashSet<VehicleCategory>(VehicleRegistrationModel.Current.VehicleCategories);
            var vehicleCategoriesCount = vehicleCategories.Count;
            var dayDifference = (endDate - startDate).Days;

            using (SqlConnection connection =
                new SqlConnection(
                    (ConfigurationManager.ConnectionStrings["VehicleRegistrationConnection"]).ConnectionString))
            {
                connection.Open();
                for (int i = 0; i < amount; i++)
                {
                    var ownerLocality = localities.ElementAt(random.Next(localitiesCount));
        
                    var registrationLocality = localities.ElementAt(random.Next(localitiesCount));
                        
                    var registrationDate = startDate.AddDays(random.Next(dayDifference));

                    var productionDate = startDate.AddDays(random.Next(dayDifference));
                    AddDateDimensionToDatabase(registrationDate, connection);
                    AddDateDimensionToDatabase(productionDate, connection);
                    
                    var fact = new RegistrationFact
                    {
                        VehicleColor = colors.ElementAt(random.Next(colorsCount)),
                        LocalityIdOfOwner = ownerLocality.LocalityId,
                        DistrictIdOfOwner = ownerLocality.DistrictId,
                        RegionIdOfOwner = ownerLocality.RegionId,
                        LocalityIdOfRegistration = registrationLocality.LocalityId,
                        DistrictIdOfRegistration = registrationLocality.DistrictId,
                        RegionIdOfRegistration = registrationLocality.RegionId,
                        VehicleModel = models.ElementAt(random.Next(modelsCount)),
                        VehicleCategory = vehicleCategories.ElementAt(random.Next(vehicleCategoriesCount)),
                        DateOfRegistrationId = registrationDate < productionDate ? productionDate : registrationDate,
                        DateOfProduceId = registrationDate < productionDate ? registrationDate : productionDate
                    };

                    try
                    {
                        AddFactToDatabase(fact, connection);
                    }
                    catch (Exception)
                    {
                    }

                }
            }
        }

//
//        private void AddDateDimensionToDatabase(DateTime dateTime)
//        {
//            try
//            {
//                dateDimensionRepository.Add(new DateDimension
//                {
//                    DateId = dateTime
//                });
//            }
//            catch (DuplicateKeyException exception)
//            {
//            }
//        }

        private void AddDateDimensionToDatabase(DateTime dateTime, SqlConnection connection)
        {
            try
            {
                new SqlCommand("Insert into DateDimension (DateId) values('" + dateTime + "')",
                    connection).ExecuteNonQuery();
            }
            catch (SqlException exception)
            {
            }
        }

        private void AddFactToDatabase(RegistrationFact fact, SqlConnection connection)
        {
            String command = String.Format("{0} '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}')", 
                @"Insert into RegistrationFact (ModelId, VehicleCategoryId, RegionIdOfRegistration, RegionIdOfOwner, LocalityIdOfRegistration, LocalityIdOfOwner, DistrictIdOfRegistration, DistrictIdOfOwner, DateOfRegistrationId, DateOfProduceId, Color) values(",
                fact.VehicleModel.ModelId, fact.VehicleCategory.VehicleCategoryId, fact.RegionIdOfRegistration,
                fact.RegionIdOfOwner, fact.LocalityIdOfRegistration, fact.LocalityIdOfOwner, 
                fact.DistrictIdOfRegistration, fact.DistrictIdOfOwner, fact.DateOfRegistrationId, fact.DateOfProduceId, fact.VehicleColor.ColorName);
                new SqlCommand(command, connection).ExecuteNonQuery();
        }

        private Dictionary<String, Double> defaultModelData = new Dictionary<string, double>
        {
            {"Volkswagen", 9.6},
            {"Opel", 5.4},
            {"Audi", 4.8},
            {"Ford", 4.7},
            {"Renault", 3.7},
            {"Peugeot", 2.7},
            {"Mercedes-Benz", 2.2},
            {"Citroen", 2},
            {"BMW", 1.96},
            {"Mazda", 1.7},
            {"Toyota", 1.5},
            {"Nissan", 1.49},
            {"Fiat", 1.3},
            {"Mitsubishi", 0.9},
            {"Hyundai", 0.77},
            {"Volvo", 0.7},
            {"Honda", 0.65},
            {"SEAT", 0.63},
            {"Kia", 0.56},
            {"Skoda", 0.49}
        };
    }
}
