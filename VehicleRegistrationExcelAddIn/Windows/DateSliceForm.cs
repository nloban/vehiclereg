﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Services;
using Services.Contracts;

namespace VehicleRegistrationExcelAddIn.Windows
{
    public partial class DateSliceForm : Form
    {
        private readonly IRegistrationFactService registrationFactService = new RegistrationFactService();

        public DateSliceForm()
        {
            InitializeComponent();
        }

        private void ExecuteButton_Click(object sender, EventArgs e)
        {
            if (StartDateTimePicker.Value >= EndDateTimePicker.Value)
            {
                MessageBox.Show("Конец периода должен быть больше начала", "Ошибка", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            else
            {
                Globals.ThisAddIn.Application.Cells.Clear();
                InitializeHeader();
                var facts = registrationFactService.GetFactsByDates(StartDateTimePicker.Value, EndDateTimePicker.Value);
                int i = 2;
                foreach (var fact in facts)
                {
                    Globals.ThisAddIn.Application.Cells[i, 1] = fact.DateOfRegistrationId.Date;
                    Globals.ThisAddIn.Application.Cells[i, 2] = fact.VehicleModel.ToString();
                    Globals.ThisAddIn.Application.Cells[i, 3] = fact.Color;
                    Globals.ThisAddIn.Application.Cells[i, 4] = fact.DateOfProduceId.ToString("Y");
                    Globals.ThisAddIn.Application.Cells[i, 5] = fact.VehicleCategoryId;
                    Globals.ThisAddIn.Application.Cells[i, 6] = fact.RegionIdOfOwner;
                    Globals.ThisAddIn.Application.Cells[i, 7] = fact.DistrictIdOfOwner;
                    Globals.ThisAddIn.Application.Cells[i, 8] = fact.LocalityIdOfOwner;
                    Globals.ThisAddIn.Application.Cells[i, 9] = fact.RegionIdOfRegistration;
                    Globals.ThisAddIn.Application.Cells[i, 10] = fact.DistrictIdOfRegistration;
                    Globals.ThisAddIn.Application.Cells[i, 11] = fact.LocalityIdOfRegistration;
                    i++;
                }

                Globals.ThisAddIn.Application.Range["A1", "K" + i].Columns.AutoFit();
            }
            
        }

        private void InitializeHeader()
        {
            var dictionary = GetName();
            Globals.ThisAddIn.Application.Cells[1, 1] = dictionary["DateOfRegistrationId"];
            Globals.ThisAddIn.Application.Cells[1, 2] = dictionary["ModelId"];
            Globals.ThisAddIn.Application.Cells[1, 3] = dictionary["Color"];
            Globals.ThisAddIn.Application.Cells[1, 4] = dictionary["DateOfProduceId"];
            Globals.ThisAddIn.Application.Cells[1, 5] = dictionary["VehicleCategoryId"];
            Globals.ThisAddIn.Application.Cells[1, 6] = dictionary["RegionIdOfOwner"];
            Globals.ThisAddIn.Application.Cells[1, 7] = dictionary["DistrictIdOfOwner"];
            Globals.ThisAddIn.Application.Cells[1, 8] = dictionary["LocalityIdOfOwner"];
            Globals.ThisAddIn.Application.Cells[1, 9] = dictionary["RegionIdOfRegistration"];
            Globals.ThisAddIn.Application.Cells[1, 10] = dictionary["DistrictIdOfRegistration"];
            Globals.ThisAddIn.Application.Cells[1, 11] = dictionary["LocalityIdOfRegistration"];
            Globals.ThisAddIn.Application.Range["A1", "K1"].Cells.Interior.Color = System.Drawing.Color.LightSteelBlue.ToArgb();
        }

        private Dictionary<String, String> GetName()
        {
            return typeof(RegistrationFact).GetProperties()
            .Where(p => p.IsDefined(typeof(DisplayAttribute), false))
            .Select(p => new
            {
                PropertyName = p.Name,
                Name = p.GetCustomAttributes(typeof(DisplayAttribute),
                    false).Cast<DisplayAttribute>().Single().Name
            }).ToDictionary(d => d.PropertyName, d => d.Name);
        }

        private void DateSliceForm_Load(object sender, EventArgs e)
        {
            StartDateTimePicker.MaxDate = DateTime.Today.AddDays(-1);
            StartDateTimePicker.MinDate = new DateTime(1884, 1, 1);
            EndDateTimePicker.MaxDate = DateTime.Today;
            EndDateTimePicker.MinDate = new DateTime(1884, 1, 2);
        }
    }
}
