﻿using System;
using System.Windows.Forms;
using VehicleRegistrationExcelAddIn.ServiceReference;

namespace VehicleRegistrationExcelAddIn.Windows
{
    public partial class DataGenerationForm : Form
    {
        public DataGenerationForm()
        {
            InitializeComponent();
        }

        private void GenerateDataButton_Click(object sender, EventArgs e)
        {
            if (AmountOfItemsComboBox.SelectedItem != null && StartDateTimePicker.Value < EndDateTimePicker.Value)
            {
                GenerateDataButton.Enabled = false;
                ExecuteQueryServiceClient client = new ExecuteQueryServiceClient();
                client.GenerateFacts(StartDateTimePicker.Value, EndDateTimePicker.Value, UInt32.Parse(AmountOfItemsComboBox.SelectedItem.ToString()));
                Close();
            } 
            else if (AmountOfItemsComboBox.SelectedItem == null)
            {
                MessageBox.Show("Выберите количество генерируемых фактов", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (StartDateTimePicker.Value >= EndDateTimePicker.Value)
            {
                MessageBox.Show("Конец периода должен быть больше начала", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DataGenerationForm_Load(object sender, EventArgs e)
        {
            AmountOfItemsComboBox.Items.AddRange(new object[] { 100, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000});
            StartDateTimePicker.MaxDate = DateTime.Today.AddDays(-1);
            StartDateTimePicker.MinDate = new DateTime(1884, 1, 1);
            EndDateTimePicker.MaxDate = DateTime.Today;
            EndDateTimePicker.MinDate = new DateTime(1884, 1, 2);
        }
    }
}
