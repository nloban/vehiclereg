﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Services;
using Services.Contracts;
using Telerik.OpenAccess.Exceptions;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    public partial class ModelAdditionForm : Form
    {
        private readonly IVehicleModelService vehicleModelService = new VehicleModelService();
        private readonly IBrandService brandService = new BrandService();
        private readonly IVehicleTypeService vehicleTypeService = new VehicleTypeService();
        private readonly IEngineTypeService engineTypeService = new EngineTypeService();

        public ModelAdditionForm()
        {
            InitializeComponent();
            InitializeBrandComboBox();
            InitializeVehicleTypeComboBox();
            InitializeEngineTypeTypeComboBox();
        }

        private void AddModelButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(ModelNameTextBox.Text) 
                && (BrandComboBox.SelectedItem != null) 
                && (VehicleTypeComboBox.SelectedItem != null)
                && (EngineTypeComboBox.SelectedItem != null)
                && StringValueVerificationService.IsStringValueCorrect(EngineSizeMaskedTextBox.Text)
                && StringValueVerificationService.IsStringValueCorrect(MaxWeightMaskedTextBox.Text)
                && StringValueVerificationService.IsStringValueCorrect(WeightMaskedTextBox.Text))
            {
                try
                {
                    AddModelButton.Enabled = false;
                    vehicleModelService.AddVehicleModel(ModelNameTextBox.Text, BrandComboBox.SelectedItem.ToString(), VehicleTypeComboBox.SelectedItem.ToString(),
                        Double.Parse(EngineSizeMaskedTextBox.Text), EngineTypeComboBox.SelectedItem.ToString(), UInt32.Parse(WeightMaskedTextBox.Text), UInt32.Parse(MaxWeightMaskedTextBox.Text));
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
//                    MessageBox.Show("Данный населенный пункт уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
//                    AddLocalityButton.Enabled = true;
                }
            }
        }

        private void InitializeBrandComboBox()
        {
            SetValueToBrandComboBox(brandService.GetAllBrands());
        }

        private void InitializeVehicleTypeComboBox()
        {
            SetValueToVehicleTypeComboBox(vehicleTypeService.GetAllVehicleTypes());
        }

        private void InitializeEngineTypeTypeComboBox()
        {
            SetValueToEngineTypeComboBox(engineTypeService.GetAllEngineTypes());
        }

        private void SetValueToBrandComboBox(List<Brand> brands)
        {
            foreach (var brand in brands.Select((x, i) => new { Index = i, Value = x }))
            {
                BrandComboBox.Items.Insert(brand.Index, brand.Value.BrandId);
            }
        }

        private void SetValueToVehicleTypeComboBox(List<VehicleType> vehicleTypes)
        {
            foreach (var vehicleType in vehicleTypes.Select((x, i) => new { Index = i, Value = x }))
            {
                VehicleTypeComboBox.Items.Insert(vehicleType.Index, vehicleType.Value.VehicleTypeId);
            }
        }

        private void SetValueToEngineTypeComboBox(List<EngineType> engineTypes)
        {
            foreach (var engineType in engineTypes.Select((x, i) => new { Index = i, Value = x }))
            {
                EngineTypeComboBox.Items.Insert(engineType.Index, engineType.Value.EngineTypeId);
            }
        }
    }
}
