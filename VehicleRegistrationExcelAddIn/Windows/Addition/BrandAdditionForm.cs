﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Services;
using Services.Contracts;
using Telerik.OpenAccess.Exceptions;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    public partial class BrandAdditionForm : Form
    {
        private readonly IBrandService brandService = new BrandService();
        private readonly ICountryOfProductionService countryOfProductionService = new CountryOfProductionService();

        public BrandAdditionForm()
        {
            InitializeComponent();
            InitializeCountryOfProductionComboBox();
        }

        private void InitializeCountryOfProductionComboBox()
        {
            var countryOfProductions = countryOfProductionService.GetAllCountryOfProductions();
            SetValuesToCountryOfProductionComboBox(countryOfProductions);
        }

        private void SetValuesToCountryOfProductionComboBox(List<CountryOfProduction> countryOfProductions)
        {
            foreach (var countryOfProduction in countryOfProductions.Select((x, i) => new { Index = i, Value = x }))
            {
                CountryOfProduceComboBox.Items.Insert(countryOfProduction.Index, countryOfProduction.Value.CountryId);
            }
        }

        private void AddBrandButton_Click(object sender, System.EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(BrandNameTextBox.Text) && (CountryOfProduceComboBox.SelectedItem != null))
            {
                try
                {
                    AddBrandButton.Enabled = false;
                    brandService.AddBrand(BrandNameTextBox.Text, CountryOfProduceComboBox.SelectedItem.ToString());
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данная марка уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddBrandButton.Enabled = true;
                }
            }
        }

        private void BrandNameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddBrandButton_Click(sender, e);
            }
        }


    }
}
