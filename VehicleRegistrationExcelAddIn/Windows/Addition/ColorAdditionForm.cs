﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Services;
using Services.Contracts;
using Telerik.OpenAccess.Exceptions;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    public partial class ColorAdditionForm : Form
    {
        private readonly IColorService colorService = new ColorService();
        public ColorAdditionForm()
        {
            InitializeComponent();
        }

        private void AddColorButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(ColorNameTextBox.Text))
            {
                try
                {
                    AddColorButton.Enabled = false;
                    colorService.AddColor(ColorNameTextBox.Text);
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данный цвет уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddColorButton.Enabled = true;
                }

            }
        }

        private void ColorNameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddColorButton_Click(sender, e);
            }
        }
    }
}
