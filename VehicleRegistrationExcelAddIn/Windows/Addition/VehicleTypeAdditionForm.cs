﻿using System.Windows.Forms;
using Services;
using Services.Contracts;
using Telerik.OpenAccess.Exceptions;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    public partial class VehicleTypeAdditionForm : Form
    {
        private readonly IVehicleTypeService vehicleTypeService = new VehicleTypeService();

        public VehicleTypeAdditionForm()
        {
            InitializeComponent();
        }

        private void AddVehicleTypeButton_Click(object sender, System.EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(VehicleTypeNameTextBox.Text))
            {
                try
                {
                    AddVehicleTypeButton.Enabled = false;
                    vehicleTypeService.AddVehicleType(VehicleTypeNameTextBox.Text);
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данный тип ТС уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddVehicleTypeButton.Enabled = true;
                }
            }
        }

        private void VehicleTypeNameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddVehicleTypeButton_Click(sender, e);
            }
        }
    }
}
