﻿namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    partial class EngineTypeAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EngineTypeTextBox = new System.Windows.Forms.TextBox();
            this.EngineTypeLabel = new System.Windows.Forms.Label();
            this.AddEngineTypeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // EngineTypeTextBox
            // 
            this.EngineTypeTextBox.Location = new System.Drawing.Point(123, 12);
            this.EngineTypeTextBox.Name = "EngineTypeTextBox";
            this.EngineTypeTextBox.Size = new System.Drawing.Size(149, 20);
            this.EngineTypeTextBox.TabIndex = 0;
            this.EngineTypeTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EngineTypeTextBox_KeyDown);
            // 
            // EngineTypeLabel
            // 
            this.EngineTypeLabel.AutoSize = true;
            this.EngineTypeLabel.Location = new System.Drawing.Point(12, 15);
            this.EngineTypeLabel.Name = "EngineTypeLabel";
            this.EngineTypeLabel.Size = new System.Drawing.Size(81, 13);
            this.EngineTypeLabel.TabIndex = 1;
            this.EngineTypeLabel.Text = "Тип двигателя";
            // 
            // AddEngineTypeButton
            // 
            this.AddEngineTypeButton.Location = new System.Drawing.Point(193, 39);
            this.AddEngineTypeButton.Name = "AddEngineTypeButton";
            this.AddEngineTypeButton.Size = new System.Drawing.Size(79, 23);
            this.AddEngineTypeButton.TabIndex = 2;
            this.AddEngineTypeButton.Text = "Добавить";
            this.AddEngineTypeButton.UseVisualStyleBackColor = true;
            this.AddEngineTypeButton.Click += new System.EventHandler(this.AddEngineTypeButton_Click);
            // 
            // EngineTypeAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 71);
            this.Controls.Add(this.AddEngineTypeButton);
            this.Controls.Add(this.EngineTypeLabel);
            this.Controls.Add(this.EngineTypeTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "EngineTypeAdditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление типа двигателя";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox EngineTypeTextBox;
        private System.Windows.Forms.Label EngineTypeLabel;
        private System.Windows.Forms.Button AddEngineTypeButton;
    }
}