﻿namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    partial class RegistrationFactAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ModelLabel = new System.Windows.Forms.Label();
            this.ModelComboBox = new System.Windows.Forms.ComboBox();
            this.VehicleCategoryLabel = new System.Windows.Forms.Label();
            this.VehicleCategoryComboBox = new System.Windows.Forms.ComboBox();
            this.ColorLabel = new System.Windows.Forms.Label();
            this.ColorComboBox = new System.Windows.Forms.ComboBox();
            this.DateOfProduceLabel = new System.Windows.Forms.Label();
            this.DateOfProduceDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.DateOfRegistrationLabel = new System.Windows.Forms.Label();
            this.DateOfRegDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.PlaceOfRegistrationGroupBox = new System.Windows.Forms.GroupBox();
            this.LocalityComboBox = new System.Windows.Forms.ComboBox();
            this.LocalityLabel = new System.Windows.Forms.Label();
            this.DistrictComboBox = new System.Windows.Forms.ComboBox();
            this.DistrictLabel = new System.Windows.Forms.Label();
            this.RegionComboBox = new System.Windows.Forms.ComboBox();
            this.RegionLabel = new System.Windows.Forms.Label();
            this.PlaceOfOwnergroupBox = new System.Windows.Forms.GroupBox();
            this.OwnerLocalityComboBox = new System.Windows.Forms.ComboBox();
            this.OwnerLocalityLabel = new System.Windows.Forms.Label();
            this.OwnerDistrictComboBox = new System.Windows.Forms.ComboBox();
            this.OwnerDistrictLabel = new System.Windows.Forms.Label();
            this.OwnerRegionComboBox = new System.Windows.Forms.ComboBox();
            this.OwnerRegionLabel = new System.Windows.Forms.Label();
            this.AddRegistrationFactButton = new System.Windows.Forms.Button();
            this.PlaceOfRegistrationGroupBox.SuspendLayout();
            this.PlaceOfOwnergroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ModelLabel
            // 
            this.ModelLabel.AutoSize = true;
            this.ModelLabel.Location = new System.Drawing.Point(13, 13);
            this.ModelLabel.Name = "ModelLabel";
            this.ModelLabel.Size = new System.Drawing.Size(46, 13);
            this.ModelLabel.TabIndex = 0;
            this.ModelLabel.Text = "Модель";
            // 
            // ModelComboBox
            // 
            this.ModelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ModelComboBox.FormattingEnabled = true;
            this.ModelComboBox.Location = new System.Drawing.Point(162, 10);
            this.ModelComboBox.Name = "ModelComboBox";
            this.ModelComboBox.Size = new System.Drawing.Size(267, 21);
            this.ModelComboBox.TabIndex = 1;
            // 
            // VehicleCategoryLabel
            // 
            this.VehicleCategoryLabel.AutoSize = true;
            this.VehicleCategoryLabel.Location = new System.Drawing.Point(12, 47);
            this.VehicleCategoryLabel.Name = "VehicleCategoryLabel";
            this.VehicleCategoryLabel.Size = new System.Drawing.Size(77, 13);
            this.VehicleCategoryLabel.TabIndex = 2;
            this.VehicleCategoryLabel.Text = "Категория ТС";
            // 
            // VehicleCategoryComboBox
            // 
            this.VehicleCategoryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.VehicleCategoryComboBox.FormattingEnabled = true;
            this.VehicleCategoryComboBox.Location = new System.Drawing.Point(162, 44);
            this.VehicleCategoryComboBox.Name = "VehicleCategoryComboBox";
            this.VehicleCategoryComboBox.Size = new System.Drawing.Size(267, 21);
            this.VehicleCategoryComboBox.TabIndex = 3;
            // 
            // ColorLabel
            // 
            this.ColorLabel.AutoSize = true;
            this.ColorLabel.Location = new System.Drawing.Point(12, 80);
            this.ColorLabel.Name = "ColorLabel";
            this.ColorLabel.Size = new System.Drawing.Size(32, 13);
            this.ColorLabel.TabIndex = 4;
            this.ColorLabel.Text = "Цвет";
            // 
            // ColorComboBox
            // 
            this.ColorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ColorComboBox.FormattingEnabled = true;
            this.ColorComboBox.Location = new System.Drawing.Point(162, 77);
            this.ColorComboBox.Name = "ColorComboBox";
            this.ColorComboBox.Size = new System.Drawing.Size(267, 21);
            this.ColorComboBox.TabIndex = 5;
            // 
            // DateOfProduceLabel
            // 
            this.DateOfProduceLabel.AutoSize = true;
            this.DateOfProduceLabel.Location = new System.Drawing.Point(12, 116);
            this.DateOfProduceLabel.Name = "DateOfProduceLabel";
            this.DateOfProduceLabel.Size = new System.Drawing.Size(107, 13);
            this.DateOfProduceLabel.TabIndex = 6;
            this.DateOfProduceLabel.Text = "Дата производства";
            // 
            // DateOfProduceDateTimePicker
            // 
            this.DateOfProduceDateTimePicker.Location = new System.Drawing.Point(162, 110);
            this.DateOfProduceDateTimePicker.Name = "DateOfProduceDateTimePicker";
            this.DateOfProduceDateTimePicker.Size = new System.Drawing.Size(267, 20);
            this.DateOfProduceDateTimePicker.TabIndex = 7;
            // 
            // DateOfRegistrationLabel
            // 
            this.DateOfRegistrationLabel.AutoSize = true;
            this.DateOfRegistrationLabel.Location = new System.Drawing.Point(12, 151);
            this.DateOfRegistrationLabel.Name = "DateOfRegistrationLabel";
            this.DateOfRegistrationLabel.Size = new System.Drawing.Size(100, 13);
            this.DateOfRegistrationLabel.TabIndex = 8;
            this.DateOfRegistrationLabel.Text = "Дата регистрации";
            // 
            // DateOfRegDateTimePicker
            // 
            this.DateOfRegDateTimePicker.Location = new System.Drawing.Point(162, 145);
            this.DateOfRegDateTimePicker.Name = "DateOfRegDateTimePicker";
            this.DateOfRegDateTimePicker.Size = new System.Drawing.Size(267, 20);
            this.DateOfRegDateTimePicker.TabIndex = 9;
            // 
            // PlaceOfRegistrationGroupBox
            // 
            this.PlaceOfRegistrationGroupBox.Controls.Add(this.LocalityComboBox);
            this.PlaceOfRegistrationGroupBox.Controls.Add(this.LocalityLabel);
            this.PlaceOfRegistrationGroupBox.Controls.Add(this.DistrictComboBox);
            this.PlaceOfRegistrationGroupBox.Controls.Add(this.DistrictLabel);
            this.PlaceOfRegistrationGroupBox.Controls.Add(this.RegionComboBox);
            this.PlaceOfRegistrationGroupBox.Controls.Add(this.RegionLabel);
            this.PlaceOfRegistrationGroupBox.Location = new System.Drawing.Point(12, 181);
            this.PlaceOfRegistrationGroupBox.Name = "PlaceOfRegistrationGroupBox";
            this.PlaceOfRegistrationGroupBox.Size = new System.Drawing.Size(417, 100);
            this.PlaceOfRegistrationGroupBox.TabIndex = 10;
            this.PlaceOfRegistrationGroupBox.TabStop = false;
            this.PlaceOfRegistrationGroupBox.Text = "Место регистрации";
            // 
            // LocalityComboBox
            // 
            this.LocalityComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LocalityComboBox.FormattingEnabled = true;
            this.LocalityComboBox.Location = new System.Drawing.Point(150, 71);
            this.LocalityComboBox.Name = "LocalityComboBox";
            this.LocalityComboBox.Size = new System.Drawing.Size(261, 21);
            this.LocalityComboBox.TabIndex = 11;
            this.LocalityComboBox.SelectionChangeCommitted += new System.EventHandler(this.LocalityComboBox_SelectionChangeCommitted);
            // 
            // LocalityLabel
            // 
            this.LocalityLabel.AutoSize = true;
            this.LocalityLabel.Location = new System.Drawing.Point(14, 74);
            this.LocalityLabel.Name = "LocalityLabel";
            this.LocalityLabel.Size = new System.Drawing.Size(102, 13);
            this.LocalityLabel.TabIndex = 10;
            this.LocalityLabel.Text = "Населенный пункт";
            // 
            // DistrictComboBox
            // 
            this.DistrictComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DistrictComboBox.FormattingEnabled = true;
            this.DistrictComboBox.Location = new System.Drawing.Point(150, 45);
            this.DistrictComboBox.Name = "DistrictComboBox";
            this.DistrictComboBox.Size = new System.Drawing.Size(261, 21);
            this.DistrictComboBox.TabIndex = 9;
            this.DistrictComboBox.SelectionChangeCommitted += new System.EventHandler(this.DistrictComboBox_SelectionChangeCommitted);
            // 
            // DistrictLabel
            // 
            this.DistrictLabel.AutoSize = true;
            this.DistrictLabel.Location = new System.Drawing.Point(14, 48);
            this.DistrictLabel.Name = "DistrictLabel";
            this.DistrictLabel.Size = new System.Drawing.Size(38, 13);
            this.DistrictLabel.TabIndex = 8;
            this.DistrictLabel.Text = "Район";
            // 
            // RegionComboBox
            // 
            this.RegionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RegionComboBox.FormattingEnabled = true;
            this.RegionComboBox.Location = new System.Drawing.Point(150, 18);
            this.RegionComboBox.Name = "RegionComboBox";
            this.RegionComboBox.Size = new System.Drawing.Size(261, 21);
            this.RegionComboBox.TabIndex = 7;
            this.RegionComboBox.SelectionChangeCommitted += new System.EventHandler(this.RegionComboBox_SelectionChangeCommitted);
            // 
            // RegionLabel
            // 
            this.RegionLabel.AutoSize = true;
            this.RegionLabel.Location = new System.Drawing.Point(14, 21);
            this.RegionLabel.Name = "RegionLabel";
            this.RegionLabel.Size = new System.Drawing.Size(50, 13);
            this.RegionLabel.TabIndex = 6;
            this.RegionLabel.Text = "Область";
            // 
            // PlaceOfOwnergroupBox
            // 
            this.PlaceOfOwnergroupBox.Controls.Add(this.OwnerLocalityComboBox);
            this.PlaceOfOwnergroupBox.Controls.Add(this.OwnerLocalityLabel);
            this.PlaceOfOwnergroupBox.Controls.Add(this.OwnerDistrictComboBox);
            this.PlaceOfOwnergroupBox.Controls.Add(this.OwnerDistrictLabel);
            this.PlaceOfOwnergroupBox.Controls.Add(this.OwnerRegionComboBox);
            this.PlaceOfOwnergroupBox.Controls.Add(this.OwnerRegionLabel);
            this.PlaceOfOwnergroupBox.Location = new System.Drawing.Point(12, 288);
            this.PlaceOfOwnergroupBox.Name = "PlaceOfOwnergroupBox";
            this.PlaceOfOwnergroupBox.Size = new System.Drawing.Size(417, 101);
            this.PlaceOfOwnergroupBox.TabIndex = 11;
            this.PlaceOfOwnergroupBox.TabStop = false;
            this.PlaceOfOwnergroupBox.Text = "Место проживания владельца";
            // 
            // OwnerLocalityComboBox
            // 
            this.OwnerLocalityComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OwnerLocalityComboBox.FormattingEnabled = true;
            this.OwnerLocalityComboBox.Location = new System.Drawing.Point(150, 73);
            this.OwnerLocalityComboBox.Name = "OwnerLocalityComboBox";
            this.OwnerLocalityComboBox.Size = new System.Drawing.Size(261, 21);
            this.OwnerLocalityComboBox.TabIndex = 11;
            this.OwnerLocalityComboBox.SelectionChangeCommitted += new System.EventHandler(this.OwnerLocalityComboBox_SelectionChangeCommitted);
            // 
            // OwnerLocalityLabel
            // 
            this.OwnerLocalityLabel.AutoSize = true;
            this.OwnerLocalityLabel.Location = new System.Drawing.Point(14, 76);
            this.OwnerLocalityLabel.Name = "OwnerLocalityLabel";
            this.OwnerLocalityLabel.Size = new System.Drawing.Size(102, 13);
            this.OwnerLocalityLabel.TabIndex = 10;
            this.OwnerLocalityLabel.Text = "Населенный пункт";
            // 
            // OwnerDistrictComboBox
            // 
            this.OwnerDistrictComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OwnerDistrictComboBox.FormattingEnabled = true;
            this.OwnerDistrictComboBox.Location = new System.Drawing.Point(150, 45);
            this.OwnerDistrictComboBox.Name = "OwnerDistrictComboBox";
            this.OwnerDistrictComboBox.Size = new System.Drawing.Size(261, 21);
            this.OwnerDistrictComboBox.TabIndex = 9;
            this.OwnerDistrictComboBox.SelectionChangeCommitted += new System.EventHandler(this.OwnerDistrictComboBox_SelectionChangeCommitted);
            // 
            // OwnerDistrictLabel
            // 
            this.OwnerDistrictLabel.AutoSize = true;
            this.OwnerDistrictLabel.Location = new System.Drawing.Point(14, 48);
            this.OwnerDistrictLabel.Name = "OwnerDistrictLabel";
            this.OwnerDistrictLabel.Size = new System.Drawing.Size(38, 13);
            this.OwnerDistrictLabel.TabIndex = 8;
            this.OwnerDistrictLabel.Text = "Район";
            // 
            // OwnerRegionComboBox
            // 
            this.OwnerRegionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OwnerRegionComboBox.FormattingEnabled = true;
            this.OwnerRegionComboBox.Location = new System.Drawing.Point(150, 16);
            this.OwnerRegionComboBox.Name = "OwnerRegionComboBox";
            this.OwnerRegionComboBox.Size = new System.Drawing.Size(261, 21);
            this.OwnerRegionComboBox.TabIndex = 7;
            this.OwnerRegionComboBox.SelectionChangeCommitted += new System.EventHandler(this.OwnerRegionComboBox_SelectionChangeCommitted);
            // 
            // OwnerRegionLabel
            // 
            this.OwnerRegionLabel.AutoSize = true;
            this.OwnerRegionLabel.Location = new System.Drawing.Point(14, 19);
            this.OwnerRegionLabel.Name = "OwnerRegionLabel";
            this.OwnerRegionLabel.Size = new System.Drawing.Size(50, 13);
            this.OwnerRegionLabel.TabIndex = 6;
            this.OwnerRegionLabel.Text = "Область";
            // 
            // AddRegistrationFactButton
            // 
            this.AddRegistrationFactButton.Location = new System.Drawing.Point(345, 395);
            this.AddRegistrationFactButton.Name = "AddRegistrationFactButton";
            this.AddRegistrationFactButton.Size = new System.Drawing.Size(84, 23);
            this.AddRegistrationFactButton.TabIndex = 12;
            this.AddRegistrationFactButton.Text = "Добавить";
            this.AddRegistrationFactButton.UseVisualStyleBackColor = true;
            this.AddRegistrationFactButton.Click += new System.EventHandler(this.AddRegistrationFactButton_Click);
            // 
            // RegistrationFactAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 427);
            this.Controls.Add(this.AddRegistrationFactButton);
            this.Controls.Add(this.PlaceOfOwnergroupBox);
            this.Controls.Add(this.PlaceOfRegistrationGroupBox);
            this.Controls.Add(this.DateOfRegDateTimePicker);
            this.Controls.Add(this.DateOfRegistrationLabel);
            this.Controls.Add(this.DateOfProduceDateTimePicker);
            this.Controls.Add(this.DateOfProduceLabel);
            this.Controls.Add(this.ColorComboBox);
            this.Controls.Add(this.ColorLabel);
            this.Controls.Add(this.VehicleCategoryComboBox);
            this.Controls.Add(this.VehicleCategoryLabel);
            this.Controls.Add(this.ModelComboBox);
            this.Controls.Add(this.ModelLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "RegistrationFactAdditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление факта регистрации";
            this.PlaceOfRegistrationGroupBox.ResumeLayout(false);
            this.PlaceOfRegistrationGroupBox.PerformLayout();
            this.PlaceOfOwnergroupBox.ResumeLayout(false);
            this.PlaceOfOwnergroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ModelLabel;
        private System.Windows.Forms.ComboBox ModelComboBox;
        private System.Windows.Forms.Label VehicleCategoryLabel;
        private System.Windows.Forms.ComboBox VehicleCategoryComboBox;
        private System.Windows.Forms.Label ColorLabel;
        private System.Windows.Forms.ComboBox ColorComboBox;
        private System.Windows.Forms.Label DateOfProduceLabel;
        private System.Windows.Forms.Label DateOfRegistrationLabel;
        private System.Windows.Forms.DateTimePicker DateOfRegDateTimePicker;
        private System.Windows.Forms.GroupBox PlaceOfRegistrationGroupBox;
        private System.Windows.Forms.GroupBox PlaceOfOwnergroupBox;
        private System.Windows.Forms.Button AddRegistrationFactButton;
        private System.Windows.Forms.Label LocalityLabel;
        private System.Windows.Forms.ComboBox DistrictComboBox;
        private System.Windows.Forms.Label DistrictLabel;
        private System.Windows.Forms.ComboBox RegionComboBox;
        private System.Windows.Forms.Label RegionLabel;
        private System.Windows.Forms.Label OwnerLocalityLabel;
        private System.Windows.Forms.ComboBox OwnerDistrictComboBox;
        private System.Windows.Forms.Label OwnerDistrictLabel;
        private System.Windows.Forms.ComboBox OwnerRegionComboBox;
        private System.Windows.Forms.Label OwnerRegionLabel;
        private System.Windows.Forms.ComboBox LocalityComboBox;
        private System.Windows.Forms.ComboBox OwnerLocalityComboBox;
        private System.Windows.Forms.DateTimePicker DateOfProduceDateTimePicker;
    }
}