﻿namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    partial class ColorAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ColorNameLabel = new System.Windows.Forms.Label();
            this.ColorNameTextBox = new System.Windows.Forms.TextBox();
            this.AddColorButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ColorNameLabel
            // 
            this.ColorNameLabel.AutoSize = true;
            this.ColorNameLabel.Location = new System.Drawing.Point(13, 13);
            this.ColorNameLabel.Name = "ColorNameLabel";
            this.ColorNameLabel.Size = new System.Drawing.Size(89, 13);
            this.ColorNameLabel.TabIndex = 0;
            this.ColorNameLabel.Text = "Название цвета";
            // 
            // ColorNameTextBox
            // 
            this.ColorNameTextBox.Location = new System.Drawing.Point(121, 13);
            this.ColorNameTextBox.Name = "ColorNameTextBox";
            this.ColorNameTextBox.Size = new System.Drawing.Size(130, 20);
            this.ColorNameTextBox.TabIndex = 1;
            this.ColorNameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ColorNameTextBox_KeyDown);
            // 
            // AddColorButton
            // 
            this.AddColorButton.Location = new System.Drawing.Point(176, 39);
            this.AddColorButton.Name = "AddColorButton";
            this.AddColorButton.Size = new System.Drawing.Size(75, 23);
            this.AddColorButton.TabIndex = 2;
            this.AddColorButton.Text = "Добавить";
            this.AddColorButton.UseVisualStyleBackColor = true;
            this.AddColorButton.Click += new System.EventHandler(this.AddColorButton_Click);
            // 
            // ColorAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 72);
            this.Controls.Add(this.AddColorButton);
            this.Controls.Add(this.ColorNameTextBox);
            this.Controls.Add(this.ColorNameLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ColorAdditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление цвета";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ColorNameLabel;
        private System.Windows.Forms.TextBox ColorNameTextBox;
        private System.Windows.Forms.Button AddColorButton;
    }
}