﻿using System.Windows.Forms;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    partial class DistrictAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RegionLabel = new System.Windows.Forms.Label();
            this.RegionComboBox = new System.Windows.Forms.ComboBox();
            this.DistrictNameLabel = new System.Windows.Forms.Label();
            this.DistrictNameTextBox = new System.Windows.Forms.TextBox();
            this.AddDictrictButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RegionLabel
            // 
            this.RegionLabel.AutoSize = true;
            this.RegionLabel.Location = new System.Drawing.Point(12, 16);
            this.RegionLabel.Name = "RegionLabel";
            this.RegionLabel.Size = new System.Drawing.Size(50, 13);
            this.RegionLabel.TabIndex = 0;
            this.RegionLabel.Text = "Область";
            // 
            // RegionComboBox
            // 
            this.RegionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RegionComboBox.FormattingEnabled = true;
            this.RegionComboBox.Location = new System.Drawing.Point(106, 13);
            this.RegionComboBox.Name = "RegionComboBox";
            this.RegionComboBox.Size = new System.Drawing.Size(139, 21);
            this.RegionComboBox.TabIndex = 1;
            // 
            // DistrictNameLabel
            // 
            this.DistrictNameLabel.AutoSize = true;
            this.DistrictNameLabel.Location = new System.Drawing.Point(12, 50);
            this.DistrictNameLabel.Name = "DistrictNameLabel";
            this.DistrictNameLabel.Size = new System.Drawing.Size(38, 13);
            this.DistrictNameLabel.TabIndex = 2;
            this.DistrictNameLabel.Text = "Район";
            // 
            // DistrictNameTextBox
            // 
            this.DistrictNameTextBox.Location = new System.Drawing.Point(106, 47);
            this.DistrictNameTextBox.Name = "DistrictNameTextBox";
            this.DistrictNameTextBox.Size = new System.Drawing.Size(139, 20);
            this.DistrictNameTextBox.TabIndex = 3;
            this.DistrictNameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DistrictNameTextBox_KeyDown);
            // 
            // AddDictrictButton
            // 
            this.AddDictrictButton.Location = new System.Drawing.Point(165, 79);
            this.AddDictrictButton.Name = "AddDictrictButton";
            this.AddDictrictButton.Size = new System.Drawing.Size(80, 23);
            this.AddDictrictButton.TabIndex = 4;
            this.AddDictrictButton.Text = "Добавить";
            this.AddDictrictButton.UseVisualStyleBackColor = true;
            this.AddDictrictButton.Click += new System.EventHandler(this.AddDictrictButton_Click);
            // 
            // DistrictAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 114);
            this.Controls.Add(this.AddDictrictButton);
            this.Controls.Add(this.DistrictNameTextBox);
            this.Controls.Add(this.DistrictNameLabel);
            this.Controls.Add(this.RegionComboBox);
            this.Controls.Add(this.RegionLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "DistrictAdditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление района";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label RegionLabel;
        private System.Windows.Forms.ComboBox RegionComboBox;
        private System.Windows.Forms.Label DistrictNameLabel;
        private System.Windows.Forms.TextBox DistrictNameTextBox;
        private System.Windows.Forms.Button AddDictrictButton;
    }
}