﻿using System.Windows.Forms;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    partial class LocalityAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RegionLabel = new System.Windows.Forms.Label();
            this.RegionComboBox = new System.Windows.Forms.ComboBox();
            this.DistrictLabel = new System.Windows.Forms.Label();
            this.DistrictComboBox = new System.Windows.Forms.ComboBox();
            this.LocalityLabel = new System.Windows.Forms.Label();
            this.LocalityTextBox = new System.Windows.Forms.TextBox();
            this.AddLocalityButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RegionLabel
            // 
            this.RegionLabel.AutoSize = true;
            this.RegionLabel.Location = new System.Drawing.Point(13, 13);
            this.RegionLabel.Name = "RegionLabel";
            this.RegionLabel.Size = new System.Drawing.Size(50, 13);
            this.RegionLabel.TabIndex = 0;
            this.RegionLabel.Text = "Область";
            // 
            // RegionComboBox
            // 
            this.RegionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RegionComboBox.FormattingEnabled = true;
            this.RegionComboBox.Location = new System.Drawing.Point(139, 10);
            this.RegionComboBox.Name = "RegionComboBox";
            this.RegionComboBox.Size = new System.Drawing.Size(163, 21);
            this.RegionComboBox.TabIndex = 1;
            this.RegionComboBox.SelectionChangeCommitted += new System.EventHandler(this.RegionComboBox_SelectionChangeCommitted);
            // 
            // DistrictLabel
            // 
            this.DistrictLabel.AutoSize = true;
            this.DistrictLabel.Location = new System.Drawing.Point(12, 40);
            this.DistrictLabel.Name = "DistrictLabel";
            this.DistrictLabel.Size = new System.Drawing.Size(38, 13);
            this.DistrictLabel.TabIndex = 2;
            this.DistrictLabel.Text = "Район";
            // 
            // DistrictComboBox
            // 
            this.DistrictComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DistrictComboBox.FormattingEnabled = true;
            this.DistrictComboBox.Location = new System.Drawing.Point(139, 37);
            this.DistrictComboBox.Name = "DistrictComboBox";
            this.DistrictComboBox.Size = new System.Drawing.Size(163, 21);
            this.DistrictComboBox.TabIndex = 3;
            this.DistrictComboBox.SelectionChangeCommitted += new System.EventHandler(this.DistrictComboBox_SelectionChangeCommitted);
            // 
            // LocalityLabel
            // 
            this.LocalityLabel.AutoSize = true;
            this.LocalityLabel.Location = new System.Drawing.Point(12, 67);
            this.LocalityLabel.Name = "LocalityLabel";
            this.LocalityLabel.Size = new System.Drawing.Size(102, 13);
            this.LocalityLabel.TabIndex = 4;
            this.LocalityLabel.Text = "Населенный пункт";
            // 
            // LocalityTextBox
            // 
            this.LocalityTextBox.Location = new System.Drawing.Point(139, 64);
            this.LocalityTextBox.Name = "LocalityTextBox";
            this.LocalityTextBox.Size = new System.Drawing.Size(163, 20);
            this.LocalityTextBox.TabIndex = 5;
            this.LocalityTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LocalityTextBox_KeyDown);
            // 
            // AddLocalityButton
            // 
            this.AddLocalityButton.Location = new System.Drawing.Point(227, 90);
            this.AddLocalityButton.Name = "AddLocalityButton";
            this.AddLocalityButton.Size = new System.Drawing.Size(75, 23);
            this.AddLocalityButton.TabIndex = 6;
            this.AddLocalityButton.Text = "Добавить";
            this.AddLocalityButton.UseVisualStyleBackColor = true;
            this.AddLocalityButton.Click += new System.EventHandler(this.AddLocalityButton_Click);
            // 
            // LocalityAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 119);
            this.Controls.Add(this.AddLocalityButton);
            this.Controls.Add(this.LocalityTextBox);
            this.Controls.Add(this.LocalityLabel);
            this.Controls.Add(this.DistrictComboBox);
            this.Controls.Add(this.DistrictLabel);
            this.Controls.Add(this.RegionComboBox);
            this.Controls.Add(this.RegionLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "LocalityAdditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление населенного пункта";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label RegionLabel;
        private System.Windows.Forms.ComboBox RegionComboBox;
        private System.Windows.Forms.Label DistrictLabel;
        private System.Windows.Forms.ComboBox DistrictComboBox;
        private System.Windows.Forms.Label LocalityLabel;
        private System.Windows.Forms.TextBox LocalityTextBox;
        private System.Windows.Forms.Button AddLocalityButton;
    }
}