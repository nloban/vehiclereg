﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Services;
using Services.Contracts;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    public partial class RegistrationFactAdditionForm : Form
    {
        private readonly IRegistrationFactService registrationFactService = new RegistrationFactService();
        private readonly IVehicleModelService vehicleModelService = new VehicleModelService();
        private readonly IVehicleCategoryService vehicleCategoryService = new VehicleCategoryService();
        private readonly IColorService colorService = new ColorService();
        private readonly ILocalityService localityService = new LocalityService();
        private readonly IDistrictService districtService = new DistrictService();
        private readonly IRegionService regionService = new RegionService();

        public RegistrationFactAdditionForm()
        {
            InitializeComponent();
            InitializeVehicleModelComboBox(ModelComboBox);
            InitializeColorComboBox(ColorComboBox);
            InitializeVehicleCategoryComboBox(VehicleCategoryComboBox);
            InitializeRegionComboBox(RegionComboBox);
            InitializeRegionComboBox(OwnerRegionComboBox);
            InitializeDistrictComboBox(DistrictComboBox);
            InitializeDistrictComboBox(OwnerDistrictComboBox);
            InitializeLocalityComboBox(LocalityComboBox);
            InitializeLocalityComboBox(OwnerLocalityComboBox);
        }

        private void AddRegistrationFactButton_Click(object sender, EventArgs e)
        {
            if ((ModelComboBox.SelectedItem != null)
                && (VehicleCategoryComboBox.SelectedItem != null)
                && (ColorComboBox.SelectedItem != null)
                && (RegionComboBox.SelectedItem != null)
                && (DistrictComboBox.SelectedItem != null)
                && (LocalityComboBox.SelectedItem != null)
                && (OwnerRegionComboBox.SelectedItem != null)
                && (OwnerDistrictComboBox.SelectedItem != null)
                && (OwnerLocalityComboBox.SelectedItem != null))
            {
                AddRegistrationFactButton.Enabled = false;
                registrationFactService.AddRegistrationFact((VehicleModel) ModelComboBox.SelectedItem, ColorComboBox.SelectedItem.ToString(),
                    VehicleCategoryComboBox.SelectedItem.ToString(), DateOfProduceDateTimePicker.Value, DateOfRegDateTimePicker.Value,
                    RegionComboBox.SelectedItem.ToString(), DistrictComboBox.SelectedItem.ToString(), LocalityComboBox.SelectedItem.ToString(),
                    OwnerRegionComboBox.SelectedItem.ToString(), OwnerDistrictComboBox.SelectedItem.ToString(), OwnerLocalityComboBox.SelectedItem.ToString());
                Close();
            }
        }

        #region Comboboxes initialization
        private void InitializeVehicleModelComboBox(ComboBox comboBox)
        {
            SetValueToComboBox(comboBox, vehicleModelService.GetAllVehicleModels().ToList());
        }
        
        private void InitializeColorComboBox(ComboBox comboBox)
        {
            SetValueToComboBox(comboBox, colorService.GetAllColors().Select(color => color.ColorName).ToList());
        }

        private void InitializeVehicleCategoryComboBox(ComboBox comboBox)
        {
            SetValueToComboBox(comboBox, vehicleCategoryService.GetAllVehicleCategories().Select(category => category.VehicleCategoryId).ToList());
        }

        private void InitializeRegionComboBox(ComboBox comboBox)
        {
            SetValueToComboBox(comboBox, regionService.GetAllRegions().Select(category => category.RegionId).ToList());
        }

        private void InitializeDistrictComboBox(ComboBox comboBox)
        {
            SetValueToComboBox(comboBox, districtService.GetAllDistricts().Select(district => district.DistrictId).ToList());
        }

        private void InitializeLocalityComboBox(ComboBox comboBox)
        {
            SetValueToComboBox(comboBox, localityService.GetAllLocalities().Select(locality => locality.LocalityId).ToList());
        }

        private void SetValueToComboBox(ComboBox comboBox, List<String> items)
        {
            comboBox.Items.Clear();
            foreach (var item in items.Distinct().ToList().Select((x, i) => new { Index = i, Value = x }))
            {
                comboBox.Items.Insert(item.Index, item.Value);
            }
        }

        private void SetValueToComboBox(ComboBox comboBox, List<VehicleModel> items)
        {
            comboBox.Items.Clear();
            foreach (var item in items.Distinct().ToList().Select((x, i) => new { Index = i, Value = x }))
            {
                comboBox.Items.Insert(item.Index, item.Value);
            }
        }
        
        #endregion
        
        #region SelectionChangeCommitted
        private void RegionComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (RegionComboBox.SelectedItem != null)
            {
                var districts = districtService.GetDistrictsByRegion(RegionComboBox.SelectedItem.ToString());
                SetValueToComboBox(DistrictComboBox, districts.Distinct().Select(d => d.DistrictId).ToList());
                var localities = localityService.GetLocalitiesByRegion(RegionComboBox.SelectedItem.ToString());
                SetValueToComboBox(LocalityComboBox, localities.Distinct().Select(d => d.LocalityId).ToList());
            }
        }

        private void DistrictComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var regions = districtService.GetRegionsByDistrict(DistrictComboBox.SelectedItem.ToString());
            var localities = localityService.GetLocalitiesByDistrict(DistrictComboBox.SelectedItem.ToString());
            SetValueToComboBox(LocalityComboBox, localities.Distinct().Select(r => r.LocalityId).ToList());
            if (regions.Count == 1)
            {
                RegionComboBox.SelectedItem = regions.First().RegionId;
            }
            else
            {
                SetValueToComboBox(RegionComboBox, regions.Distinct().Select(r => r.RegionId).ToList());
            }
        }

        private void LocalityComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var districts = localityService.GetDistrictsByLocality(LocalityComboBox.SelectedItem.ToString());
            SetValueToComboBox(RegionComboBox, districts.Select(d => d.RegionId).Distinct().ToList());
            if (districts.Count == 1)
            {
                var district = districts.First();
                DistrictComboBox.SelectedItem = district.DistrictId;
                var regions = districtService.GetRegionsByDistrict(district.DistrictId);
                if (regions.Count == 1)
                {
                    RegionComboBox.SelectedItem = regions.First().RegionId;
                }
                
            }
            else
            {
                SetValueToComboBox(DistrictComboBox, districts.Distinct().Select(d => d.DistrictId).ToList());
            }
        }
        
        private void OwnerRegionComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (OwnerRegionComboBox.SelectedItem != null)
            {
                var districts = districtService.GetDistrictsByRegion(OwnerRegionComboBox.SelectedItem.ToString());
                SetValueToComboBox(OwnerDistrictComboBox, districts.Distinct().Select(d => d.DistrictId).ToList());
                var localities = localityService.GetLocalitiesByRegion(OwnerRegionComboBox.SelectedItem.ToString());
                SetValueToComboBox(OwnerLocalityComboBox, localities.Distinct().Select(d => d.LocalityId).ToList());
            }
        }

        private void OwnerDistrictComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var regions = districtService.GetRegionsByDistrict(OwnerDistrictComboBox.SelectedItem.ToString());
            var localities = localityService.GetLocalitiesByDistrict(OwnerDistrictComboBox.SelectedItem.ToString());
            SetValueToComboBox(OwnerLocalityComboBox, localities.Distinct().Select(r => r.LocalityId).ToList());
            if (regions.Count == 1)
            {
                OwnerRegionComboBox.SelectedItem = regions.First().RegionId;
            }
            else
            {
                SetValueToComboBox(OwnerRegionComboBox, regions.Distinct().Select(region => region.RegionId).ToList());
            }
        }

        private void OwnerLocalityComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var districts = localityService.GetDistrictsByLocality(OwnerLocalityComboBox.SelectedItem.ToString());
            SetValueToComboBox(OwnerRegionComboBox, districts.Select(d => d.RegionId).Distinct().ToList());
            if (districts.Count == 1)
            {
                var district = districts.First();
                OwnerDistrictComboBox.SelectedItem = district.DistrictId;
                var regions = districtService.GetRegionsByDistrict(district.DistrictId);
                if (regions.Count == 1)
                {
                    OwnerRegionComboBox.SelectedItem = regions.First().RegionId;
                }

            }
            else
            {
                SetValueToComboBox(OwnerDistrictComboBox, districts.Distinct().Select(d => d.DistrictId).ToList());
            }
        }

        #endregion
    }
}
