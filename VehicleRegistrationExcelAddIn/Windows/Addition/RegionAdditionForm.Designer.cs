﻿namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    partial class RegionAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddRegionButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.RegionNameTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // AddRegionButton
            // 
            this.AddRegionButton.Location = new System.Drawing.Point(212, 35);
            this.AddRegionButton.Name = "AddRegionButton";
            this.AddRegionButton.Size = new System.Drawing.Size(75, 23);
            this.AddRegionButton.TabIndex = 0;
            this.AddRegionButton.Text = "Добавить";
            this.AddRegionButton.UseVisualStyleBackColor = true;
            this.AddRegionButton.Click += new System.EventHandler(this.AddRegionButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Название области";
            // 
            // RegionNameTextBox
            // 
            this.RegionNameTextBox.Location = new System.Drawing.Point(119, 9);
            this.RegionNameTextBox.Name = "RegionNameTextBox";
            this.RegionNameTextBox.Size = new System.Drawing.Size(168, 20);
            this.RegionNameTextBox.TabIndex = 2;
            this.RegionNameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RegionNameTextBox_KeyDown);
            // 
            // RegionAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 66);
            this.Controls.Add(this.RegionNameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddRegionButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "RegionAdditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление области";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddRegionButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RegionNameTextBox;
    }
}