﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Services;
using Services.Contracts;
using Telerik.OpenAccess.Exceptions;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    public partial class EngineTypeAdditionForm : Form
    {
        private readonly IEngineTypeService engineTypeService = new EngineTypeService();

        public EngineTypeAdditionForm()
        {
            InitializeComponent();
        }

        private void AddEngineTypeButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(EngineTypeTextBox.Text))
            {
                try
                {
                    AddEngineTypeButton.Enabled = false;
                    engineTypeService.AddEngineType(EngineTypeTextBox.Text);
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данный тип двигателя уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddEngineTypeButton.Enabled = true;
                }
            }
        }

        private void EngineTypeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddEngineTypeButton_Click(sender, e);
            }
        }
    }
}
