﻿namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    partial class VehicleTypeAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.VehicleTypeNameLabel = new System.Windows.Forms.Label();
            this.AddVehicleTypeButton = new System.Windows.Forms.Button();
            this.VehicleTypeNameTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // VehicleTypeNameLabel
            // 
            this.VehicleTypeNameLabel.AutoSize = true;
            this.VehicleTypeNameLabel.Location = new System.Drawing.Point(12, 13);
            this.VehicleTypeNameLabel.Name = "VehicleTypeNameLabel";
            this.VehicleTypeNameLabel.Size = new System.Drawing.Size(100, 13);
            this.VehicleTypeNameLabel.TabIndex = 1;
            this.VehicleTypeNameLabel.Text = "Название типа ТС";
            // 
            // AddVehicleTypeButton
            // 
            this.AddVehicleTypeButton.Location = new System.Drawing.Point(212, 36);
            this.AddVehicleTypeButton.Name = "AddVehicleTypeButton";
            this.AddVehicleTypeButton.Size = new System.Drawing.Size(75, 23);
            this.AddVehicleTypeButton.TabIndex = 2;
            this.AddVehicleTypeButton.Text = "Добавить";
            this.AddVehicleTypeButton.UseVisualStyleBackColor = true;
            this.AddVehicleTypeButton.Click += new System.EventHandler(this.AddVehicleTypeButton_Click);
            // 
            // VehicleTypeNameTextBox
            // 
            this.VehicleTypeNameTextBox.Location = new System.Drawing.Point(118, 10);
            this.VehicleTypeNameTextBox.Name = "VehicleTypeNameTextBox";
            this.VehicleTypeNameTextBox.Size = new System.Drawing.Size(169, 20);
            this.VehicleTypeNameTextBox.TabIndex = 3;
            this.VehicleTypeNameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.VehicleTypeNameTextBox_KeyDown);
            // 
            // VehicleTypeAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 67);
            this.Controls.Add(this.VehicleTypeNameTextBox);
            this.Controls.Add(this.AddVehicleTypeButton);
            this.Controls.Add(this.VehicleTypeNameLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "VehicleTypeAdditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление типа ТС";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label VehicleTypeNameLabel;
        private System.Windows.Forms.Button AddVehicleTypeButton;
        private System.Windows.Forms.TextBox VehicleTypeNameTextBox;

    }
}