﻿namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    partial class BrandAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CountryOfProduceLabel = new System.Windows.Forms.Label();
            this.BrandNameLabel = new System.Windows.Forms.Label();
            this.CountryOfProduceComboBox = new System.Windows.Forms.ComboBox();
            this.BrandNameTextBox = new System.Windows.Forms.TextBox();
            this.AddBrandButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CountryOfProduceLabel
            // 
            this.CountryOfProduceLabel.AutoSize = true;
            this.CountryOfProduceLabel.Location = new System.Drawing.Point(13, 13);
            this.CountryOfProduceLabel.Name = "CountryOfProduceLabel";
            this.CountryOfProduceLabel.Size = new System.Drawing.Size(123, 13);
            this.CountryOfProduceLabel.TabIndex = 0;
            this.CountryOfProduceLabel.Text = "Страна-производитель";
            // 
            // BrandNameLabel
            // 
            this.BrandNameLabel.AutoSize = true;
            this.BrandNameLabel.Location = new System.Drawing.Point(12, 45);
            this.BrandNameLabel.Name = "BrandNameLabel";
            this.BrandNameLabel.Size = new System.Drawing.Size(92, 13);
            this.BrandNameLabel.TabIndex = 1;
            this.BrandNameLabel.Text = "Название марки";
            // 
            // CountryOfProduceComboBox
            // 
            this.CountryOfProduceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CountryOfProduceComboBox.FormattingEnabled = true;
            this.CountryOfProduceComboBox.Location = new System.Drawing.Point(142, 10);
            this.CountryOfProduceComboBox.Name = "CountryOfProduceComboBox";
            this.CountryOfProduceComboBox.Size = new System.Drawing.Size(176, 21);
            this.CountryOfProduceComboBox.TabIndex = 2;
            // 
            // BrandNameTextBox
            // 
            this.BrandNameTextBox.Location = new System.Drawing.Point(142, 42);
            this.BrandNameTextBox.Name = "BrandNameTextBox";
            this.BrandNameTextBox.Size = new System.Drawing.Size(176, 20);
            this.BrandNameTextBox.TabIndex = 3;
            this.BrandNameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BrandNameTextBox_KeyDown);
            // 
            // AddBrandButton
            // 
            this.AddBrandButton.Location = new System.Drawing.Point(243, 68);
            this.AddBrandButton.Name = "AddBrandButton";
            this.AddBrandButton.Size = new System.Drawing.Size(75, 23);
            this.AddBrandButton.TabIndex = 4;
            this.AddBrandButton.Text = "Добавить";
            this.AddBrandButton.UseVisualStyleBackColor = true;
            this.AddBrandButton.Click += new System.EventHandler(this.AddBrandButton_Click);
            // 
            // BrandAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 98);
            this.Controls.Add(this.AddBrandButton);
            this.Controls.Add(this.BrandNameTextBox);
            this.Controls.Add(this.CountryOfProduceComboBox);
            this.Controls.Add(this.BrandNameLabel);
            this.Controls.Add(this.CountryOfProduceLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "BrandAdditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление марки ТС";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CountryOfProduceLabel;
        private System.Windows.Forms.Label BrandNameLabel;
        private System.Windows.Forms.ComboBox CountryOfProduceComboBox;
        private System.Windows.Forms.TextBox BrandNameTextBox;
        private System.Windows.Forms.Button AddBrandButton;
    }
}