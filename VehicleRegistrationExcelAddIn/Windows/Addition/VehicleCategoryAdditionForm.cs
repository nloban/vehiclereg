﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Services;
using Services.Contracts;
using Telerik.OpenAccess.Exceptions;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    public partial class VehicleCategoryAdditionForm : Form
    {
        private readonly IVehicleCategoryService vehicleCategoryService = new VehicleCategoryService();

        public VehicleCategoryAdditionForm()
        {
            InitializeComponent();
        }
        
        private void AddVehicleCategoryButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(VehicleCategoryTextBox.Text))
            {
                try
                {
                    AddVehicleCategoryButton.Enabled = false;
                    vehicleCategoryService.AddVehicleCategory(VehicleCategoryTextBox.Text);
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данная категория ТС уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddVehicleCategoryButton.Enabled = true;
                }
            }
        }

        private void VehicleCategoryTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddVehicleCategoryButton_Click(sender, e);
            }
        }
    }
}
