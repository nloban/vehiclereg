﻿using System;
using System.Linq;
using System.Windows.Forms;
using Services;
using Services.Contracts;
using Telerik.OpenAccess.Exceptions;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    public partial class DistrictAdditionForm : Form
    {
        private readonly IRegionService regionService = new RegionService();
        private readonly IDistrictService districtService = new DistrictService();

        public DistrictAdditionForm()
        {
            InitializeComponent();
            InitializeRegionComboBox();
        }

        private void AddDictrictButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(DistrictNameTextBox.Text) && (RegionComboBox.SelectedItem != null))
            {
                try
                {
                    AddDictrictButton.Enabled = false;
                    districtService.AddDistrict(DistrictNameTextBox.Text, RegionComboBox.SelectedItem.ToString());
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данный район уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddDictrictButton.Enabled = true;
                }
            }
        }

        private void DistrictNameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddDictrictButton_Click(sender, e);
            }
        }

        private void InitializeRegionComboBox()
        {
            var regions = regionService.GetAllRegions();

            foreach (var region in regions.Select((x, i) => new { Index = i, Value = x }))
            {
                RegionComboBox.Items.Insert(region.Index, region.Value.RegionId);
            }
        }
    }
}
