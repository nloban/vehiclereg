﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Services;
using Services.Contracts;
using Telerik.OpenAccess.Exceptions;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    public partial class RegionAdditionForm : Form
    {
        private readonly IRegionService regionService= new RegionService();
        public RegionAdditionForm()
        {
            InitializeComponent();
        }

        private void AddRegionButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(RegionNameTextBox.Text))
            {
                try
                {
                    AddRegionButton.Enabled = false;
                    regionService.AddRegion(RegionNameTextBox.Text);
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данная область уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddRegionButton.Enabled = true;
                }
            }
        }

        private void RegionNameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddRegionButton_Click(sender, e);
            }
        }
    }
}
