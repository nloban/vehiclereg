﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Services;
using Services.Contracts;
using Telerik.OpenAccess.Exceptions;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    public partial class LocalityAdditionForm : Form
    {
        private readonly ILocalityService localityService = new LocalityService();
        private readonly IDistrictService districtService = new DistrictService();
        private readonly IRegionService regionService = new RegionService();

        public LocalityAdditionForm()
        {
            InitializeComponent();
            InitializeRegionComboBox();
            InitializeDistrictComboBox();
        }

        private void AddLocalityButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(LocalityTextBox.Text) && (RegionComboBox.SelectedItem != null) && (DistrictComboBox.SelectedItem != null))
            {
                try
                {
                    AddLocalityButton.Enabled = false;
                    localityService.AddLocality(LocalityTextBox.Text, DistrictComboBox.SelectedItem.ToString(), RegionComboBox.SelectedItem.ToString());
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данный населенный пункт уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddLocalityButton.Enabled = true;
                }
            }
        }

        private void InitializeRegionComboBox()
        {
            var regions = regionService.GetAllRegions();
            SetValueToRegionComboBox(regions);
        }

        private void RegionComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (RegionComboBox.SelectedItem != null)
            {
                var districts = districtService.GetDistrictsByRegion(RegionComboBox.SelectedItem.ToString());

                SetValuesToDistrictComboBox(districts);
            }
        }

        private void DistrictComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var regions = districtService.GetRegionsByDistrict(DistrictComboBox.SelectedItem.ToString());
            if (regions.Count == 1)
            {
                RegionComboBox.SelectedItem = regions.First().RegionId;
            }
            else
            {
                SetValueToRegionComboBox(regions);
            }
        }

        private void InitializeDistrictComboBox()
        {
            var districts = districtService.GetAllDistricts();
            SetValuesToDistrictComboBox(districts);
        }

        private void SetValuesToDistrictComboBox(List<District> districts)
        {
            DistrictComboBox.Items.Clear();
            foreach (var district in districts.Select((x, i) => new { Index = i, Value = x }))
            {
                DistrictComboBox.Items.Insert(district.Index, district.Value.DistrictId);
            }
        }

        private void SetValueToRegionComboBox(List<Region> regions)
        {
            foreach (var region in regions.Select((x, i) => new { Index = i, Value = x }))
            {
                RegionComboBox.Items.Insert(region.Index, region.Value.RegionId);
            }
        }

        private void LocalityTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddLocalityButton_Click(sender, e);
            }
        }

    }
}
