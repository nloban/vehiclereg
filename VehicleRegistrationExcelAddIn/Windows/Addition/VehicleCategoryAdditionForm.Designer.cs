﻿namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    partial class VehicleCategoryAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.VehicleCategoryLabel = new System.Windows.Forms.Label();
            this.VehicleCategoryTextBox = new System.Windows.Forms.TextBox();
            this.AddVehicleCategoryButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // VehicleCategoryLabel
            // 
            this.VehicleCategoryLabel.AutoSize = true;
            this.VehicleCategoryLabel.Location = new System.Drawing.Point(12, 16);
            this.VehicleCategoryLabel.Name = "VehicleCategoryLabel";
            this.VehicleCategoryLabel.Size = new System.Drawing.Size(129, 13);
            this.VehicleCategoryLabel.TabIndex = 0;
            this.VehicleCategoryLabel.Text = "Название категории ТС";
            // 
            // VehicleCategoryTextBox
            // 
            this.VehicleCategoryTextBox.Location = new System.Drawing.Point(164, 13);
            this.VehicleCategoryTextBox.Name = "VehicleCategoryTextBox";
            this.VehicleCategoryTextBox.Size = new System.Drawing.Size(102, 20);
            this.VehicleCategoryTextBox.TabIndex = 1;
            this.VehicleCategoryTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.VehicleCategoryTextBox_KeyDown);
            // 
            // AddVehicleCategoryButton
            // 
            this.AddVehicleCategoryButton.Location = new System.Drawing.Point(201, 39);
            this.AddVehicleCategoryButton.Name = "AddVehicleCategoryButton";
            this.AddVehicleCategoryButton.Size = new System.Drawing.Size(65, 23);
            this.AddVehicleCategoryButton.TabIndex = 2;
            this.AddVehicleCategoryButton.Text = "Добавить";
            this.AddVehicleCategoryButton.UseVisualStyleBackColor = true;
            this.AddVehicleCategoryButton.Click += new System.EventHandler(this.AddVehicleCategoryButton_Click);
            // 
            // VehicleCategoryAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 66);
            this.Controls.Add(this.AddVehicleCategoryButton);
            this.Controls.Add(this.VehicleCategoryTextBox);
            this.Controls.Add(this.VehicleCategoryLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "VehicleCategoryAdditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление категории ТС";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label VehicleCategoryLabel;
        private System.Windows.Forms.TextBox VehicleCategoryTextBox;
        private System.Windows.Forms.Button AddVehicleCategoryButton;
    }
}