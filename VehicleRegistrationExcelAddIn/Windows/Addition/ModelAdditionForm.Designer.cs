﻿namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    partial class ModelAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BrandLabel = new System.Windows.Forms.Label();
            this.ModelLabel = new System.Windows.Forms.Label();
            this.VehicleTypeLabel = new System.Windows.Forms.Label();
            this.EngineTypeLabel = new System.Windows.Forms.Label();
            this.EngineSizeLabel = new System.Windows.Forms.Label();
            this.WeightLabel = new System.Windows.Forms.Label();
            this.MaxWeightLabel = new System.Windows.Forms.Label();
            this.BrandComboBox = new System.Windows.Forms.ComboBox();
            this.ModelNameTextBox = new System.Windows.Forms.TextBox();
            this.VehicleTypeComboBox = new System.Windows.Forms.ComboBox();
            this.EngineSizeMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.EngineTypeComboBox = new System.Windows.Forms.ComboBox();
            this.WeightMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.MaxWeightMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.AddModelButton = new System.Windows.Forms.Button();
            this.LLabel = new System.Windows.Forms.Label();
            this.KgLabel = new System.Windows.Forms.Label();
            this.MaxWKgLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BrandLabel
            // 
            this.BrandLabel.AutoSize = true;
            this.BrandLabel.Location = new System.Drawing.Point(12, 9);
            this.BrandLabel.Name = "BrandLabel";
            this.BrandLabel.Size = new System.Drawing.Size(40, 13);
            this.BrandLabel.TabIndex = 2;
            this.BrandLabel.Text = "Марка";
            // 
            // ModelLabel
            // 
            this.ModelLabel.AutoSize = true;
            this.ModelLabel.Location = new System.Drawing.Point(12, 35);
            this.ModelLabel.Name = "ModelLabel";
            this.ModelLabel.Size = new System.Drawing.Size(46, 13);
            this.ModelLabel.TabIndex = 3;
            this.ModelLabel.Text = "Модель";
            // 
            // VehicleTypeLabel
            // 
            this.VehicleTypeLabel.AutoSize = true;
            this.VehicleTypeLabel.Location = new System.Drawing.Point(12, 60);
            this.VehicleTypeLabel.Name = "VehicleTypeLabel";
            this.VehicleTypeLabel.Size = new System.Drawing.Size(43, 13);
            this.VehicleTypeLabel.TabIndex = 4;
            this.VehicleTypeLabel.Text = "Тип ТС";
            // 
            // EngineTypeLabel
            // 
            this.EngineTypeLabel.AutoSize = true;
            this.EngineTypeLabel.Location = new System.Drawing.Point(12, 85);
            this.EngineTypeLabel.Name = "EngineTypeLabel";
            this.EngineTypeLabel.Size = new System.Drawing.Size(81, 13);
            this.EngineTypeLabel.TabIndex = 5;
            this.EngineTypeLabel.Text = "Тип двигателя";
            // 
            // EngineSizeLabel
            // 
            this.EngineSizeLabel.AutoSize = true;
            this.EngineSizeLabel.Location = new System.Drawing.Point(12, 112);
            this.EngineSizeLabel.Name = "EngineSizeLabel";
            this.EngineSizeLabel.Size = new System.Drawing.Size(97, 13);
            this.EngineSizeLabel.TabIndex = 6;
            this.EngineSizeLabel.Text = "Объем двигателя";
            // 
            // WeightLabel
            // 
            this.WeightLabel.AutoSize = true;
            this.WeightLabel.Location = new System.Drawing.Point(12, 138);
            this.WeightLabel.Name = "WeightLabel";
            this.WeightLabel.Size = new System.Drawing.Size(40, 13);
            this.WeightLabel.TabIndex = 7;
            this.WeightLabel.Text = "Масса";
            // 
            // MaxWeightLabel
            // 
            this.MaxWeightLabel.AutoSize = true;
            this.MaxWeightLabel.Location = new System.Drawing.Point(12, 161);
            this.MaxWeightLabel.Name = "MaxWeightLabel";
            this.MaxWeightLabel.Size = new System.Drawing.Size(102, 26);
            this.MaxWeightLabel.TabIndex = 8;
            this.MaxWeightLabel.Text = "Максимально \r\nдопустимая масса";
            // 
            // BrandComboBox
            // 
            this.BrandComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BrandComboBox.FormattingEnabled = true;
            this.BrandComboBox.Location = new System.Drawing.Point(137, 6);
            this.BrandComboBox.Name = "BrandComboBox";
            this.BrandComboBox.Size = new System.Drawing.Size(129, 21);
            this.BrandComboBox.TabIndex = 9;
            // 
            // ModelNameTextBox
            // 
            this.ModelNameTextBox.Location = new System.Drawing.Point(137, 32);
            this.ModelNameTextBox.Name = "ModelNameTextBox";
            this.ModelNameTextBox.Size = new System.Drawing.Size(129, 20);
            this.ModelNameTextBox.TabIndex = 10;
            // 
            // VehicleTypeComboBox
            // 
            this.VehicleTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.VehicleTypeComboBox.FormattingEnabled = true;
            this.VehicleTypeComboBox.Location = new System.Drawing.Point(137, 57);
            this.VehicleTypeComboBox.Name = "VehicleTypeComboBox";
            this.VehicleTypeComboBox.Size = new System.Drawing.Size(129, 21);
            this.VehicleTypeComboBox.TabIndex = 11;
            // 
            // EngineSizeMaskedTextBox
            // 
            this.EngineSizeMaskedTextBox.Location = new System.Drawing.Point(137, 109);
            this.EngineSizeMaskedTextBox.Mask = "0.0";
            this.EngineSizeMaskedTextBox.Name = "EngineSizeMaskedTextBox";
            this.EngineSizeMaskedTextBox.Size = new System.Drawing.Size(109, 20);
            this.EngineSizeMaskedTextBox.TabIndex = 12;
            // 
            // EngineTypeComboBox
            // 
            this.EngineTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EngineTypeComboBox.FormattingEnabled = true;
            this.EngineTypeComboBox.Location = new System.Drawing.Point(137, 82);
            this.EngineTypeComboBox.Name = "EngineTypeComboBox";
            this.EngineTypeComboBox.Size = new System.Drawing.Size(129, 21);
            this.EngineTypeComboBox.TabIndex = 13;
            // 
            // WeightMaskedTextBox
            // 
            this.WeightMaskedTextBox.Location = new System.Drawing.Point(137, 135);
            this.WeightMaskedTextBox.Mask = "000000";
            this.WeightMaskedTextBox.Name = "WeightMaskedTextBox";
            this.WeightMaskedTextBox.Size = new System.Drawing.Size(109, 20);
            this.WeightMaskedTextBox.TabIndex = 14;
            // 
            // MaxWeightMaskedTextBox
            // 
            this.MaxWeightMaskedTextBox.Location = new System.Drawing.Point(137, 161);
            this.MaxWeightMaskedTextBox.Mask = "000000";
            this.MaxWeightMaskedTextBox.Name = "MaxWeightMaskedTextBox";
            this.MaxWeightMaskedTextBox.Size = new System.Drawing.Size(109, 20);
            this.MaxWeightMaskedTextBox.TabIndex = 15;
            // 
            // AddModelButton
            // 
            this.AddModelButton.Location = new System.Drawing.Point(190, 188);
            this.AddModelButton.Name = "AddModelButton";
            this.AddModelButton.Size = new System.Drawing.Size(75, 23);
            this.AddModelButton.TabIndex = 16;
            this.AddModelButton.Text = "Добавить";
            this.AddModelButton.UseVisualStyleBackColor = true;
            this.AddModelButton.Click += new System.EventHandler(this.AddModelButton_Click);
            // 
            // LLabel
            // 
            this.LLabel.AutoSize = true;
            this.LLabel.Location = new System.Drawing.Point(248, 112);
            this.LLabel.Name = "LLabel";
            this.LLabel.Size = new System.Drawing.Size(13, 13);
            this.LLabel.TabIndex = 17;
            this.LLabel.Text = "л";
            // 
            // KgLabel
            // 
            this.KgLabel.AutoSize = true;
            this.KgLabel.Location = new System.Drawing.Point(248, 138);
            this.KgLabel.Name = "KgLabel";
            this.KgLabel.Size = new System.Drawing.Size(18, 13);
            this.KgLabel.TabIndex = 18;
            this.KgLabel.Text = "кг";
            // 
            // MaxWKgLabel
            // 
            this.MaxWKgLabel.AutoSize = true;
            this.MaxWKgLabel.Location = new System.Drawing.Point(248, 164);
            this.MaxWKgLabel.Name = "MaxWKgLabel";
            this.MaxWKgLabel.Size = new System.Drawing.Size(18, 13);
            this.MaxWKgLabel.TabIndex = 19;
            this.MaxWKgLabel.Text = "кг";
            // 
            // ModelAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(273, 223);
            this.Controls.Add(this.MaxWKgLabel);
            this.Controls.Add(this.KgLabel);
            this.Controls.Add(this.LLabel);
            this.Controls.Add(this.AddModelButton);
            this.Controls.Add(this.MaxWeightMaskedTextBox);
            this.Controls.Add(this.WeightMaskedTextBox);
            this.Controls.Add(this.EngineTypeComboBox);
            this.Controls.Add(this.EngineSizeMaskedTextBox);
            this.Controls.Add(this.VehicleTypeComboBox);
            this.Controls.Add(this.ModelNameTextBox);
            this.Controls.Add(this.BrandComboBox);
            this.Controls.Add(this.MaxWeightLabel);
            this.Controls.Add(this.WeightLabel);
            this.Controls.Add(this.EngineSizeLabel);
            this.Controls.Add(this.EngineTypeLabel);
            this.Controls.Add(this.VehicleTypeLabel);
            this.Controls.Add(this.ModelLabel);
            this.Controls.Add(this.BrandLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ModelAdditionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление модели";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label BrandLabel;
        private System.Windows.Forms.Label ModelLabel;
        private System.Windows.Forms.Label VehicleTypeLabel;
        private System.Windows.Forms.Label EngineTypeLabel;
        private System.Windows.Forms.Label EngineSizeLabel;
        private System.Windows.Forms.Label WeightLabel;
        private System.Windows.Forms.Label MaxWeightLabel;
        private System.Windows.Forms.ComboBox BrandComboBox;
        private System.Windows.Forms.TextBox ModelNameTextBox;
        private System.Windows.Forms.ComboBox VehicleTypeComboBox;
        private System.Windows.Forms.MaskedTextBox EngineSizeMaskedTextBox;
        private System.Windows.Forms.ComboBox EngineTypeComboBox;
        private System.Windows.Forms.MaskedTextBox WeightMaskedTextBox;
        private System.Windows.Forms.MaskedTextBox MaxWeightMaskedTextBox;
        private System.Windows.Forms.Button AddModelButton;
        private System.Windows.Forms.Label LLabel;
        private System.Windows.Forms.Label KgLabel;
        private System.Windows.Forms.Label MaxWKgLabel;
    }
}