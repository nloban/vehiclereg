﻿namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    partial class GetYearForChartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ShowChartButton = new System.Windows.Forms.Button();
            this.YearLabel = new System.Windows.Forms.Label();
            this.YeatComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // ShowChartButton
            // 
            this.ShowChartButton.Location = new System.Drawing.Point(154, 46);
            this.ShowChartButton.Name = "ShowChartButton";
            this.ShowChartButton.Size = new System.Drawing.Size(75, 23);
            this.ShowChartButton.TabIndex = 0;
            this.ShowChartButton.Text = "Построить";
            this.ShowChartButton.UseVisualStyleBackColor = true;
            this.ShowChartButton.Click += new System.EventHandler(this.ShowChartButton_Click);
            // 
            // YearLabel
            // 
            this.YearLabel.AutoSize = true;
            this.YearLabel.Location = new System.Drawing.Point(13, 13);
            this.YearLabel.Name = "YearLabel";
            this.YearLabel.Size = new System.Drawing.Size(25, 13);
            this.YearLabel.TabIndex = 1;
            this.YearLabel.Text = "Год";
            // 
            // YeatComboBox
            // 
            this.YeatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.YeatComboBox.FormattingEnabled = true;
            this.YeatComboBox.Location = new System.Drawing.Point(107, 10);
            this.YeatComboBox.Name = "YeatComboBox";
            this.YeatComboBox.Size = new System.Drawing.Size(122, 21);
            this.YeatComboBox.TabIndex = 2;
            // 
            // GetYearForChartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(241, 79);
            this.Controls.Add(this.YeatComboBox);
            this.Controls.Add(this.YearLabel);
            this.Controls.Add(this.ShowChartButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "GetYearForChartForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Построение графика";
            this.Load += new System.EventHandler(this.GetYearForChartForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ShowChartButton;
        private System.Windows.Forms.Label YearLabel;
        private System.Windows.Forms.ComboBox YeatComboBox;
    }
}