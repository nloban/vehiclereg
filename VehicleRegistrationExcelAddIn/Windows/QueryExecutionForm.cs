﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Services;
using VehicleRegistrationExcelAddIn.ServiceReference;

namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    public partial class QueryExecutionForm : Form
    {
        public QueryExecutionForm()
        {
            InitializeComponent();
        }

        private void ExecuteQueryButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(QueryTextBox.Text))
            {
                try
                {

                    ExecuteQueryButton.Enabled = false;
                    ServiceReference.ExecuteQueryServiceClient client = new ExecuteQueryServiceClient();
                    var result = client.ExecuteSelectStatement(QueryTextBox.Text);
                    if (result.Equals("Проверьте правильность введенного запроса!"))
                    {
                        MessageBox.Show("Проверьте правильность введенного запроса!", "Ошибка", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);

                        ExecuteQueryButton.Enabled = true;
                    }
                    else if (result.Equals("Введеннное выражение не является SELECT запросом!"))
                    {
                        MessageBox.Show("Введеннное выражение не является SELECT запросом!", "Ошибка", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        ExecuteQueryButton.Enabled = true;
                    }
                    else
                    {
                        Globals.ThisAddIn.Application.Cells.Clear();


                        String[] record = result.Split('\n');
                        for (int i = 0; i < record.Length - 1; i++)
                        {
                            String[] cellStr = record[i].Split('|');
                            for (int j = 0; j < cellStr.Length - 1; j++)
                            {
                                Globals.ThisAddIn.Application.Cells[i + 1, j + 1] = cellStr[j];
                            }
                        }
                        Globals.ThisAddIn.Application.Cells[1, record[1].Length].Interior.Color =
                            System.Drawing.Color.LightSteelBlue.ToArgb();
                        Close();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Неверный синтаксис запроса", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void QueryTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ExecuteQueryButton_Click(sender, e);
            }
        }
    }
}
