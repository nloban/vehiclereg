﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Services;
using Services.Contracts;


namespace VehicleRegistrationExcelAddIn.Windows.Addition
{
    public partial class GetYearForChartForm : Form
    {
        private readonly IDateDimensionService dateDimensionService = new DateDimensionService();

        public GetYearForChartForm()
        {
            InitializeComponent();
        }

        private void GetYearForChartForm_Load(object sender, EventArgs e)
        {
            var minYear = dateDimensionService.GetMinRegistrationDate().Year;
            var maxYear = dateDimensionService.GetMaxRegistrationDate().Year;

            for (int i = 0; i <= (maxYear - minYear); i++)
            {
                YeatComboBox.Items.Insert(i, minYear + i);
            }
        }

        private void ShowChartButton_Click(object sender, EventArgs e)
        {
            if (YeatComboBox.SelectedItem != null)
            {
                ShowChartButton.Enabled = false;
                Globals.ThisAddIn.Application.Cells.Clear();
                var values = dateDimensionService.GetAmountOfRegistered(Int32.Parse(YeatComboBox.SelectedItem.ToString()));
                CreateChart(values);
                Close();
            }
        }

        private void CreateChart(Dictionary<Int32, Int32> chartValues)
        {
            Worksheet worksheet = (Worksheet)Globals.ThisAddIn.Application.Sheets[1];

            var chartObjects = (ChartObjects)worksheet.ChartObjects();
            ChartObject chartObject = chartObjects.Add(200, 20, 500, 300);
            Chart excelChart = chartObject.Chart;
            excelChart.HasTitle = true;
            excelChart.ChartTitle.Text = String.Format("{0} ({1} год)","Количество зарегистрированных ТС по месяцам", YeatComboBox.SelectedItem);

            excelChart.ChartType = XlChartType.xlLineStacked;

            excelChart.Axes(XlAxisType.xlCategory).HasTitle = true;
            excelChart.Axes(XlAxisType.xlCategory).AxisTitle.Text = "Месяцы";
            excelChart.Axes(XlAxisType.xlValue).HasTitle = true;
            excelChart.Axes(XlAxisType.xlValue).AxisTitle.Text = "Количество транспортных средств";
            worksheet.Cells[1, 2] = "Месяцы";
            worksheet.Cells[1, 3] = "Количество ТС";

            for (int row = 0; row < 12; row++)
            {
                var date = new DateTime(1, row + 1, 1);
                worksheet.Cells[row + 2, 2] = date.ToString("MMMM", CultureInfo.CurrentCulture);
                worksheet.Cells[row + 2, 3] = chartValues[row + 1];
            }

            worksheet.Range["B1", "C1"].Cells.Interior.Color = System.Drawing.Color.DarkSalmon.ToArgb();

            worksheet.Range["B1", "B13"].Columns.AutoFit();
            worksheet.Range["C1", "C13"].Columns.AutoFit();

            Range xValues = worksheet.Range["B2", "B13"];
            Range values = worksheet.Range["C2", "C13"];
            SeriesCollection seriesCollection = excelChart.SeriesCollection();

            Series series = seriesCollection.NewSeries();
            series.XValues = xValues;
            series.Values = values;
            series.Name = "Количество ТС";

            Globals.ThisAddIn.Application.Visible = true;
        }
    }
}
