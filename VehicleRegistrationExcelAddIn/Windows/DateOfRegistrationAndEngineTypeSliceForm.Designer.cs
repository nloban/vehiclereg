﻿namespace VehicleRegistrationExcelAddIn.Windows
{
    partial class DateOfRegistrationAndEngineTypeSliceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartYearComboBox = new System.Windows.Forms.ComboBox();
            this.ExecuteButton = new System.Windows.Forms.Button();
            this.StartYearLabel = new System.Windows.Forms.Label();
            this.EndYearLabel = new System.Windows.Forms.Label();
            this.EndYearComboBox = new System.Windows.Forms.ComboBox();
            this.MonthCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // StartYearComboBox
            // 
            this.StartYearComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StartYearComboBox.FormattingEnabled = true;
            this.StartYearComboBox.Location = new System.Drawing.Point(210, 10);
            this.StartYearComboBox.Name = "StartYearComboBox";
            this.StartYearComboBox.Size = new System.Drawing.Size(188, 21);
            this.StartYearComboBox.TabIndex = 0;
            this.StartYearComboBox.SelectionChangeCommitted += new System.EventHandler(this.StartYearComboBox_SelectionChangeCommitted);
            // 
            // ExecuteButton
            // 
            this.ExecuteButton.Location = new System.Drawing.Point(303, 117);
            this.ExecuteButton.Name = "ExecuteButton";
            this.ExecuteButton.Size = new System.Drawing.Size(95, 23);
            this.ExecuteButton.TabIndex = 1;
            this.ExecuteButton.Text = "Выполнить";
            this.ExecuteButton.UseVisualStyleBackColor = true;
            this.ExecuteButton.Click += new System.EventHandler(this.ExecuteButton_Click);
            // 
            // StartYearLabel
            // 
            this.StartYearLabel.AutoSize = true;
            this.StartYearLabel.Location = new System.Drawing.Point(13, 13);
            this.StartYearLabel.Name = "StartYearLabel";
            this.StartYearLabel.Size = new System.Drawing.Size(129, 13);
            this.StartYearLabel.TabIndex = 2;
            this.StartYearLabel.Text = "Начальный год анализа";
            // 
            // EndYearLabel
            // 
            this.EndYearLabel.AutoSize = true;
            this.EndYearLabel.Location = new System.Drawing.Point(13, 49);
            this.EndYearLabel.Name = "EndYearLabel";
            this.EndYearLabel.Size = new System.Drawing.Size(122, 13);
            this.EndYearLabel.TabIndex = 3;
            this.EndYearLabel.Text = "Конечный год анализа";
            // 
            // EndYearComboBox
            // 
            this.EndYearComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EndYearComboBox.FormattingEnabled = true;
            this.EndYearComboBox.Location = new System.Drawing.Point(210, 46);
            this.EndYearComboBox.Name = "EndYearComboBox";
            this.EndYearComboBox.Size = new System.Drawing.Size(188, 21);
            this.EndYearComboBox.TabIndex = 4;
            this.EndYearComboBox.SelectionChangeCommitted += new System.EventHandler(this.EndYearComboBox_SelectionChangeCommitted);
            // 
            // MonthCheckBox
            // 
            this.MonthCheckBox.AutoSize = true;
            this.MonthCheckBox.Location = new System.Drawing.Point(16, 93);
            this.MonthCheckBox.Name = "MonthCheckBox";
            this.MonthCheckBox.Size = new System.Drawing.Size(213, 17);
            this.MonthCheckBox.TabIndex = 5;
            this.MonthCheckBox.Text = "Подробная информация по месяцам";
            this.MonthCheckBox.UseVisualStyleBackColor = true;
            // 
            // DateOfRegistrationAndEngineTypeSliceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 153);
            this.Controls.Add(this.MonthCheckBox);
            this.Controls.Add(this.EndYearComboBox);
            this.Controls.Add(this.EndYearLabel);
            this.Controls.Add(this.StartYearLabel);
            this.Controls.Add(this.ExecuteButton);
            this.Controls.Add(this.StartYearComboBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "DateOfRegistrationAndEngineTypeSliceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Срез данных по дате регистрации и типу двигателя";
            this.Load += new System.EventHandler(this.DateOfRegistrationAndEngineTypeSliceForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox StartYearComboBox;
        private System.Windows.Forms.Button ExecuteButton;
        private System.Windows.Forms.Label StartYearLabel;
        private System.Windows.Forms.Label EndYearLabel;
        private System.Windows.Forms.ComboBox EndYearComboBox;
        private System.Windows.Forms.CheckBox MonthCheckBox;
    }
}