﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Services;
using Services.Contracts;
using Telerik.OpenAccess.RT.OID;

namespace VehicleRegistrationExcelAddIn.Windows
{
    public partial class DateOfRegistrationAndEngineTypeSliceForm : Form
    {
        private readonly IDateDimensionService dateDimensionService = new DateDimensionService();
        private readonly IRegistrationFactService registrationFactService = new RegistrationFactService();
        private readonly IEngineTypeService engineTypeService = new EngineTypeService();
        private Int32 minYear;
        private Int32 maxYear;

        private List<EngineType> engineTypes;

        public DateOfRegistrationAndEngineTypeSliceForm()
        {
            InitializeComponent();
        }

        private void ExecuteButton_Click(object sender, EventArgs e)
        {
            if (StartYearComboBox.SelectedItem != null && EndYearComboBox.SelectedItem != null)
            {
                ExecuteButton.Enabled = false;
                Globals.ThisAddIn.Application.Cells.Clear();
                
                var facts =
                    registrationFactService.GetFactsByYears(Int32.Parse(StartYearComboBox.SelectedItem.ToString()),
                        Int32.Parse(EndYearComboBox.SelectedItem.ToString()));
                ShowSlice(facts, MonthCheckBox.Checked);
            }
        }

        private void StartYearComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var selectedYear = Int32.Parse(StartYearComboBox.SelectedItem.ToString());
            if (selectedYear == maxYear)
            {
                EndYearComboBox.SelectedItem = maxYear;
            }
            else
            {
                EndYearComboBox.Items.Clear();
                for (int i = selectedYear - minYear; i <= maxYear - minYear; i++)
                {
                    EndYearComboBox.Items.Insert(i - (selectedYear - minYear), minYear + i);
                }
            }
        }

        private void EndYearComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Int32.Parse(EndYearComboBox.SelectedItem.ToString()) == minYear)
            {
                StartYearComboBox.SelectedItem = minYear;
            }
            else
            {
                if (StartYearComboBox.SelectedItem == null ||
                    Int32.Parse(StartYearComboBox.SelectedItem.ToString()) >
                    Int32.Parse(EndYearComboBox.SelectedItem.ToString()))
                {
                    StartYearComboBox.Items.Clear();
                    for (int i = 0; i <= (Int32.Parse(EndYearComboBox.SelectedItem.ToString()) - minYear); i++)
                    {
                        StartYearComboBox.Items.Insert(i, minYear + i);
                    }
                }
            }
        }

        private void DateOfRegistrationAndEngineTypeSliceForm_Load(object sender, EventArgs e)
        {
            Globals.ThisAddIn.Application.Cells.Clear();
            minYear = dateDimensionService.GetMinRegistrationDate().Year;
            maxYear = dateDimensionService.GetMaxRegistrationDate().Year;
            engineTypes = engineTypeService.GetAllEngineTypes().OrderBy(type => type.EngineTypeId).ToList();

            for (int i = 0; i <= (maxYear - minYear); i++)
            {
                StartYearComboBox.Items.Insert(i, minYear + i);
                EndYearComboBox.Items.Insert(i, minYear + i);
            }
        }

        private void ShowSlice(Dictionary<Int32, List<RegistrationFact>> dictionary, bool idMonthSlice)
        {
            InitializeHeaders(dictionary, idMonthSlice);
            InitializeData(dictionary, idMonthSlice);
        }

        private void InitializeData(Dictionary<Int32, List<RegistrationFact>> dictionary, bool idMonthSlice)
        {
            int width = (idMonthSlice ? 3 : 2) + engineTypes.Count;
            int height = 1;
            int i = idMonthSlice ? 3 : 2;
            foreach (var engineType in engineTypes)
            {
                Int32 commonSumByEngineType = 0;
                int j = 2;
                foreach (var item in dictionary)
                {
                    var facts = item.Value.Where(fact => fact.VehicleModel.EngineTypeId.Equals(engineType.EngineTypeId));
                    var count = facts.Count();
                    if (idMonthSlice)
                    {
                        for (int m = 0; m < ((item.Key == DateTime.Today.Year) ? DateTime.Today.Month : 12); m++)
                        {
                            var countByMonth = facts.Where(fact => fact.DateOfRegistrationId.Month == (m + 1)).Count();
                            Globals.ThisAddIn.Application.Cells[j, i] = countByMonth;
                            Globals.ThisAddIn.Application.Cells[j, (idMonthSlice ? 3 : 2) + engineTypes.Count] = item.Value.Where(fact => fact.DateOfRegistrationId.Month == (m + 1)).Count();
                            Globals.ThisAddIn.Application.Cells[j, (idMonthSlice ? 3 : 2) + engineTypes.Count].Interior.Color = System.Drawing.Color.LimeGreen.ToArgb();
                            j++;
                        }
                    }
                    Globals.ThisAddIn.Application.Cells[j, (idMonthSlice ? 3 : 2) + engineTypes.Count] = item.Value.Count;
                    Globals.ThisAddIn.Application.Cells[j, i] = count;
                    commonSumByEngineType += count; 
                    j++;
                }
                Globals.ThisAddIn.Application.Cells[j, i] = commonSumByEngineType;
                Globals.ThisAddIn.Application.Cells[j, i].Interior.Color = System.Drawing.Color.DarkSalmon.ToArgb();
                height = j;
                i++;
            }

            Globals.ThisAddIn.Application.Cells[height, width] = dictionary.Select(facts => facts.Value.Count).Sum();
            Globals.ThisAddIn.Application.Cells[height, width].Interior.Color = System.Drawing.Color.MediumSpringGreen.ToArgb();

            Globals.ThisAddIn.Application.Range["A1", GetExcelColumnName(width, height)].Columns.AutoFit();
               
        }

        private void InitializeHeaders(Dictionary<Int32, List<RegistrationFact>> dictionary, bool idMonthSlice)
        {
            int width = (idMonthSlice ? 3 : 2) + engineTypes.Count;
            int height = 1;
            int i = idMonthSlice ? 3 : 2;
            foreach (var engineType in engineTypes)
            {
                Globals.ThisAddIn.Application.Cells[1, i] = engineType.EngineTypeId;
                Globals.ThisAddIn.Application.Cells[1, i].Interior.Color = System.Drawing.Color.LightSkyBlue.ToArgb();
                i++;
            }

            Globals.ThisAddIn.Application.Cells[1, i] = "Всего за период";
            Globals.ThisAddIn.Application.Cells[1, i].Interior.Color = System.Drawing.Color.Orchid.ToArgb();

            int j = 2;
            foreach (var item in dictionary)
            {
                Globals.ThisAddIn.Application.Cells[j, 1] = item.Key;
                Globals.ThisAddIn.Application.Cells[j, 1].HorizontalAlignment = Constants.xlLeft;
                Globals.ThisAddIn.Application.Cells[j, 1].Interior.Color = System.Drawing.Color.GreenYellow.ToArgb();
                if (idMonthSlice)
                {
                    for (int m = 0; m < ((item.Key == DateTime.Today.Year) ? DateTime.Today.Month : 12); m++)
                    {
                        var date = new DateTime(1, m + 1, 1);
                        Globals.ThisAddIn.Application.Cells[j, 2] = date.ToString("MMMM", CultureInfo.CurrentCulture);
                        Globals.ThisAddIn.Application.Cells[j, 2].Interior.Color = System.Drawing.Color.LightSteelBlue.ToArgb();
                        j++;
                    }
                    Globals.ThisAddIn.Application.Cells[j, 1] = "Всего";
                    Globals.ThisAddIn.Application.Range[GetExcelColumnName(1, j), GetExcelColumnName(width - 1, j)].Cells.Interior.Color = System.Drawing.Color.Violet.ToArgb();
                    Globals.ThisAddIn.Application.Cells[j, width].Interior.Color = System.Drawing.Color.LightSalmon.ToArgb();
                }
                j++;
            }
            height = j;
            Globals.ThisAddIn.Application.Cells[j, 1] = "Всего по типу двигателя";
            Globals.ThisAddIn.Application.Range[GetExcelColumnName(1, j), GetExcelColumnName(2, j)].Cells.Interior.Color = System.Drawing.Color.DarkSalmon.ToArgb();
        }

        private string GetExcelColumnName(int columnNumber, int rowNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return String.Format("{0}{1}", columnName, rowNumber);
        }
    }
}
