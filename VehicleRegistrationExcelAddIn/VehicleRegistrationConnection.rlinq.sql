-- VehicleRegistrationExcelAddIn.Brand
CREATE TABLE [Brand] (
    [BrandId] varchar(255) NOT NULL,        -- _brandId
    [CountryId] varchar(255) NULL,          -- countryOfProduction
    CONSTRAINT [pk_Brand] PRIMARY KEY ([BrandId])
)
go

-- VehicleRegistrationExcelAddIn.Color
CREATE TABLE [Color] (
    [ColorName] varchar(255) NOT NULL,      -- _colorName
    CONSTRAINT [pk_Color] PRIMARY KEY ([ColorName])
)
go

-- VehicleRegistrationExcelAddIn.CountryOfProduction
CREATE TABLE [CountryOfProduction] (
    [CountryId] varchar(255) NOT NULL,      -- _countryId
    CONSTRAINT [pk_CountryOfProduction] PRIMARY KEY ([CountryId])
)
go

-- VehicleRegistrationExcelAddIn.DateDimension
CREATE TABLE [DateDimension] (
    [DateId] datetime NOT NULL,             -- _dateId
    CONSTRAINT [pk_DateDimension] PRIMARY KEY ([DateId])
)
go

-- VehicleRegistrationExcelAddIn.District
CREATE TABLE [District] (
    [DistrictId] varchar(255) NOT NULL,     -- _districtId
    [RegionId] varchar(255) NOT NULL,       -- _regionId
    CONSTRAINT [pk_District] PRIMARY KEY ([DistrictId], [RegionId])
)
go

-- VehicleRegistrationExcelAddIn.EngineType
CREATE TABLE [EngineType] (
    [EngineTypeId] varchar(255) NOT NULL,   -- _engineTypeId
    CONSTRAINT [pk_EngineType] PRIMARY KEY ([EngineTypeId])
)
go

-- VehicleRegistrationExcelAddIn.Locality
CREATE TABLE [Locality] (
    [LocalityId] varchar(255) NOT NULL,     -- _localityId
    [DistrictId] varchar(255) NOT NULL,     -- _districtId
    [RegionId] varchar(255) NOT NULL,       -- _regionId
    CONSTRAINT [pk_Locality] PRIMARY KEY ([DistrictId], [LocalityId], [RegionId])
)
go

-- VehicleRegistrationExcelAddIn.Region
CREATE TABLE [Region] (
    [RegionId] varchar(255) NOT NULL,       -- _regionId
    CONSTRAINT [pk_Region] PRIMARY KEY ([RegionId])
)
go

-- VehicleRegistrationExcelAddIn.RegistrationFact
CREATE TABLE [RegistrationFact] (
    [ModelId] bigint NOT NULL,              -- _modelId
    [VehicleCategoryId] varchar(255) NOT NULL, -- _vehicleCategoryId
    [RegionIdOfRegistration] varchar(255) NOT NULL, -- _regionIdOfRegistration
    [RegionIdOfOwner] varchar(255) NOT NULL, -- _regionIdOfOwner
    [LocalityIdOfRegistration] varchar(255) NOT NULL, -- _localityIdOfRegistration
    [LocalityIdOfOwner] varchar(255) NOT NULL, -- _localityIdOfOwner
    [DistrictIdOfRegistration] varchar(255) NOT NULL, -- _districtIdOfRegistration
    [DistrictIdOfOwner] varchar(255) NOT NULL, -- _districtIdOfOwner
    [DateOfRegistrationId] datetime NOT NULL, -- _dateOfRegistrationId
    [DateOfProduceId] datetime NOT NULL,    -- _dateOfProduce
    [Color] varchar(255) NOT NULL,          -- _color
    CONSTRAINT [pk_RegistrationFact] PRIMARY KEY ([Color], [DateOfProduceId], [DateOfRegistrationId], [DistrictIdOfOwner], [DistrictIdOfRegistration], [LocalityIdOfOwner], [LocalityIdOfRegistration], [ModelId], [RegionIdOfOwner], [RegionIdOfRegistration], [VehicleCategoryId])
)
go

-- VehicleRegistrationExcelAddIn.VehicleCategory
CREATE TABLE [VehicleCategory] (
    [VehicleCategoryId] varchar(255) NOT NULL, -- _vehicleCategoryId
    CONSTRAINT [pk_VehicleCategory] PRIMARY KEY ([VehicleCategoryId])
)
go

-- VehicleRegistrationExcelAddIn.VehicleModel
CREATE TABLE [VehicleModel] (
    [ModelId] bigint IDENTITY NOT NULL,     -- _modelId
    [NameOfModel] varchar(255) NULL,        -- _nameOfModel
    [BrandId] varchar(255) NULL,            -- brand
    [VehicleTypeId] varchar(255) NULL,      -- vehicleType
    [EngineTypeId] varchar(255) NULL,       -- engineType
    [EngineSize] float NOT NULL,            -- engineSize
    [Weight] bigint NOT NULL,               -- _weight
    [MaxPermittedWeight] bigint NOT NULL,   -- _maxPermittedWeight
    CONSTRAINT [pk_VehicleModel] PRIMARY KEY ([ModelId])
)
go

-- VehicleRegistrationExcelAddIn.VehicleType
CREATE TABLE [VehicleType] (
    [VehicleTypeId] varchar(255) NOT NULL,  -- _vehicleTypeId
    CONSTRAINT [pk_VehicleType] PRIMARY KEY ([VehicleTypeId])
)
go

CREATE INDEX [idx_Brand_CountryId] ON [Brand]([CountryId])
go

CREATE INDEX [idx_District_RegionId] ON [District]([RegionId])
go

CREATE INDEX [idx_Lclity_DistrictId_RegionId] ON [Locality]([DistrictId], [RegionId])
go

CREATE INDEX [idx_RgstrtnFct_VhcleCategoryId] ON [RegistrationFact]([VehicleCategoryId])
go

CREATE INDEX [idx_RgstrtnFct_DtOfRgstrtionId] ON [RegistrationFact]([DateOfRegistrationId])
go

CREATE INDEX [idx_RgstrtnFct_DstrctIdOfRgstr] ON [RegistrationFact]([DistrictIdOfRegistration], [LocalityIdOfRegistration], [RegionIdOfRegistration])
go

CREATE INDEX [idx_RgstrtnFct_DstrctIdOfOwnr_] ON [RegistrationFact]([DistrictIdOfOwner], [LocalityIdOfOwner], [RegionIdOfOwner])
go

CREATE INDEX [idx_RgstrtnFct_DateOfProduceId] ON [RegistrationFact]([DateOfProduceId])
go

CREATE INDEX [idx_RegistrationFact_ModelId] ON [RegistrationFact]([ModelId])
go

CREATE INDEX [idx_RegistrationFact_Color] ON [RegistrationFact]([Color])
go

CREATE INDEX [idx_VehicleModel_VehicleTypeId] ON [VehicleModel]([VehicleTypeId])
go

CREATE INDEX [idx_VehicleModel_EngineTypeId] ON [VehicleModel]([EngineTypeId])
go

CREATE INDEX [idx_VehicleModel_BrandId] ON [VehicleModel]([BrandId])
go

ALTER TABLE [Brand] ADD CONSTRAINT [ref_Brand_CountryOfProduction] FOREIGN KEY ([CountryId]) REFERENCES [CountryOfProduction]([CountryId])
go

ALTER TABLE [District] ADD CONSTRAINT [ref_District_Region] FOREIGN KEY ([RegionId]) REFERENCES [Region]([RegionId])
go

ALTER TABLE [Locality] ADD CONSTRAINT [ref_Locality_District] FOREIGN KEY ([DistrictId], [RegionId]) REFERENCES [District]([DistrictId], [RegionId])
go

ALTER TABLE [RegistrationFact] ADD CONSTRAINT [ref_RegistrationFact_Color] FOREIGN KEY ([Color]) REFERENCES [Color]([ColorName])
go

ALTER TABLE [RegistrationFact] ADD CONSTRAINT [ref_RgstrtonFact_DateDimension] FOREIGN KEY ([DateOfProduceId]) REFERENCES [DateDimension]([DateId])
go

ALTER TABLE [RegistrationFact] ADD CONSTRAINT [ref_RgstrtonFact_DateDimensio2] FOREIGN KEY ([DateOfRegistrationId]) REFERENCES [DateDimension]([DateId])
go

ALTER TABLE [RegistrationFact] ADD CONSTRAINT [ref_RegistrationFact_Locality] FOREIGN KEY ([DistrictIdOfRegistration], [LocalityIdOfRegistration], [RegionIdOfRegistration]) REFERENCES [Locality]([DistrictId], [LocalityId], [RegionId])
go

ALTER TABLE [RegistrationFact] ADD CONSTRAINT [ref_RegistrationFact_Locality2] FOREIGN KEY ([DistrictIdOfOwner], [LocalityIdOfOwner], [RegionIdOfOwner]) REFERENCES [Locality]([DistrictId], [LocalityId], [RegionId])
go

ALTER TABLE [RegistrationFact] ADD CONSTRAINT [ref_RgstrtnFct_VehicleCategory] FOREIGN KEY ([VehicleCategoryId]) REFERENCES [VehicleCategory]([VehicleCategoryId])
go

ALTER TABLE [RegistrationFact] ADD CONSTRAINT [ref_RgstrtionFact_VehicleModel] FOREIGN KEY ([ModelId]) REFERENCES [VehicleModel]([ModelId])
go

ALTER TABLE [VehicleModel] ADD CONSTRAINT [ref_VehicleModel_Brand] FOREIGN KEY ([BrandId]) REFERENCES [Brand]([BrandId])
go

ALTER TABLE [VehicleModel] ADD CONSTRAINT [ref_VehicleModel_EngineType] FOREIGN KEY ([EngineTypeId]) REFERENCES [EngineType]([EngineTypeId])
go

ALTER TABLE [VehicleModel] ADD CONSTRAINT [ref_VehicleModel_VehicleType] FOREIGN KEY ([VehicleTypeId]) REFERENCES [VehicleType]([VehicleTypeId])
go

