﻿using System;
using DataModelAccess;
using DataModelAccess.Contracts;
using DataModelAccess.Repositories;
using Ninject.Modules;
using Services;
using Services.Contracts;

namespace VehicleRegistrationExcelAddIn.Ninject
{
    class VehicleRegistrationNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IBrandRepository>().To<BrandRepository>();
            Bind<IColorRepository>().To<ColorRepository>();
            Bind<IDbCreationService>().To<DbCreationService>();
            Bind<IColorService>().To<ColorService>();
        }
    }
}
