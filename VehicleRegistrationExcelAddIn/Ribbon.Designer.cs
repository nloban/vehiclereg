﻿namespace VehicleRegistrationExcelAddIn
{
    partial class Ribbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.VehicleRegLoban = this.Factory.CreateRibbonTab();
            this.DataGenerationGroup = this.Factory.CreateRibbonGroup();
            this.GenerateDataButton = this.Factory.CreateRibbonButton();
            this.AddRegistrationFactButton = this.Factory.CreateRibbonButton();
            this.DataAdditionGroup = this.Factory.CreateRibbonGroup();
            this.AddModelButton = this.Factory.CreateRibbonButton();
            this.AddVehicleCategoryButton = this.Factory.CreateRibbonButton();
            this.AddColorButton = this.Factory.CreateRibbonButton();
            this.separator1 = this.Factory.CreateRibbonSeparator();
            this.AddVehicleTypeButton = this.Factory.CreateRibbonButton();
            this.AddEngineTypeButton = this.Factory.CreateRibbonButton();
            this.AddBrandButton = this.Factory.CreateRibbonButton();
            this.separator3 = this.Factory.CreateRibbonSeparator();
            this.AddRegionButton = this.Factory.CreateRibbonButton();
            this.AddDistrictButton = this.Factory.CreateRibbonButton();
            this.LocalityButton = this.Factory.CreateRibbonButton();
            this.SqlQueryGroup = this.Factory.CreateRibbonGroup();
            this.ExecuteQueryButton = this.Factory.CreateRibbonButton();
            this.GraphGroup = this.Factory.CreateRibbonGroup();
            this.ShowChartButton = this.Factory.CreateRibbonButton();
            this.SGroup = this.Factory.CreateRibbonGroup();
            this.DateSliceButton = this.Factory.CreateRibbonButton();
            this.DateAndEngineTypeSliceButton = this.Factory.CreateRibbonButton();
            this.VehicleAgeSliceButton = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.VehicleRegLoban.SuspendLayout();
            this.DataGenerationGroup.SuspendLayout();
            this.DataAdditionGroup.SuspendLayout();
            this.SqlQueryGroup.SuspendLayout();
            this.GraphGroup.SuspendLayout();
            this.SGroup.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // VehicleRegLoban
            // 
            this.VehicleRegLoban.Groups.Add(this.DataGenerationGroup);
            this.VehicleRegLoban.Groups.Add(this.DataAdditionGroup);
            this.VehicleRegLoban.Groups.Add(this.SqlQueryGroup);
            this.VehicleRegLoban.Groups.Add(this.GraphGroup);
            this.VehicleRegLoban.Groups.Add(this.SGroup);
            this.VehicleRegLoban.Label = "Лобан Н. А.";
            this.VehicleRegLoban.Name = "VehicleRegLoban";
            // 
            // DataGenerationGroup
            // 
            this.DataGenerationGroup.Items.Add(this.GenerateDataButton);
            this.DataGenerationGroup.Items.Add(this.AddRegistrationFactButton);
            this.DataGenerationGroup.Label = "Генерация данных";
            this.DataGenerationGroup.Name = "DataGenerationGroup";
            // 
            // GenerateDataButton
            // 
            this.GenerateDataButton.Label = "Сгенерировать данные";
            this.GenerateDataButton.Name = "GenerateDataButton";
            this.GenerateDataButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.GenerateDataButton_Click);
            // 
            // AddRegistrationFactButton
            // 
            this.AddRegistrationFactButton.Label = "Добавить факт регистрации";
            this.AddRegistrationFactButton.Name = "AddRegistrationFactButton";
            this.AddRegistrationFactButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddRegistrationFactButton_Click);
            // 
            // DataAdditionGroup
            // 
            this.DataAdditionGroup.Items.Add(this.AddModelButton);
            this.DataAdditionGroup.Items.Add(this.AddVehicleCategoryButton);
            this.DataAdditionGroup.Items.Add(this.AddColorButton);
            this.DataAdditionGroup.Items.Add(this.separator1);
            this.DataAdditionGroup.Items.Add(this.AddVehicleTypeButton);
            this.DataAdditionGroup.Items.Add(this.AddEngineTypeButton);
            this.DataAdditionGroup.Items.Add(this.AddBrandButton);
            this.DataAdditionGroup.Items.Add(this.separator3);
            this.DataAdditionGroup.Items.Add(this.AddRegionButton);
            this.DataAdditionGroup.Items.Add(this.AddDistrictButton);
            this.DataAdditionGroup.Items.Add(this.LocalityButton);
            this.DataAdditionGroup.Label = "Добавление измерений";
            this.DataAdditionGroup.Name = "DataAdditionGroup";
            // 
            // AddModelButton
            // 
            this.AddModelButton.Label = "Модель";
            this.AddModelButton.Name = "AddModelButton";
            this.AddModelButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddModelButton_Click);
            // 
            // AddVehicleCategoryButton
            // 
            this.AddVehicleCategoryButton.Label = "Категория ТС";
            this.AddVehicleCategoryButton.Name = "AddVehicleCategoryButton";
            this.AddVehicleCategoryButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddVehicleCategoryButton_Click);
            // 
            // AddColorButton
            // 
            this.AddColorButton.Label = "Цвет";
            this.AddColorButton.Name = "AddColorButton";
            this.AddColorButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddColorButton_Click);
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            // 
            // AddVehicleTypeButton
            // 
            this.AddVehicleTypeButton.Label = "Тип ТС";
            this.AddVehicleTypeButton.Name = "AddVehicleTypeButton";
            this.AddVehicleTypeButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddVehicleTypeButton_Click);
            // 
            // AddEngineTypeButton
            // 
            this.AddEngineTypeButton.Label = "Тип двигателя";
            this.AddEngineTypeButton.Name = "AddEngineTypeButton";
            this.AddEngineTypeButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddEngineTypeButton_Click);
            // 
            // AddBrandButton
            // 
            this.AddBrandButton.Label = "Марка";
            this.AddBrandButton.Name = "AddBrandButton";
            this.AddBrandButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddBrandButton_Click);
            // 
            // separator3
            // 
            this.separator3.Name = "separator3";
            // 
            // AddRegionButton
            // 
            this.AddRegionButton.Label = "Область";
            this.AddRegionButton.Name = "AddRegionButton";
            this.AddRegionButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddRegionButton_Click);
            // 
            // AddDistrictButton
            // 
            this.AddDistrictButton.Label = "Район";
            this.AddDistrictButton.Name = "AddDistrictButton";
            this.AddDistrictButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddDistrictButton_Click);
            // 
            // LocalityButton
            // 
            this.LocalityButton.Label = "Населенный пункт";
            this.LocalityButton.Name = "LocalityButton";
            this.LocalityButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.LocalityButton_Click);
            // 
            // SqlQueryGroup
            // 
            this.SqlQueryGroup.Items.Add(this.ExecuteQueryButton);
            this.SqlQueryGroup.Label = "SQL запрос";
            this.SqlQueryGroup.Name = "SqlQueryGroup";
            // 
            // ExecuteQueryButton
            // 
            this.ExecuteQueryButton.Label = "Выполнить SQL запрос";
            this.ExecuteQueryButton.Name = "ExecuteQueryButton";
            this.ExecuteQueryButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ExecuteQueryButton_Click);
            // 
            // GraphGroup
            // 
            this.GraphGroup.Items.Add(this.ShowChartButton);
            this.GraphGroup.Label = "Графики";
            this.GraphGroup.Name = "GraphGroup";
            // 
            // ShowChartButton
            // 
            this.ShowChartButton.Label = "График изменения количества ТС за год";
            this.ShowChartButton.Name = "ShowChartButton";
            this.ShowChartButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ShowChartButton_Click);
            // 
            // SGroup
            // 
            this.SGroup.Items.Add(this.DateSliceButton);
            this.SGroup.Items.Add(this.DateAndEngineTypeSliceButton);
            this.SGroup.Items.Add(this.VehicleAgeSliceButton);
            this.SGroup.Label = "Срезы";
            this.SGroup.Name = "SGroup";
            // 
            // DateSliceButton
            // 
            this.DateSliceButton.Label = "Факты регистрации за период";
            this.DateSliceButton.Name = "DateSliceButton";
            this.DateSliceButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.DateSliceButton_Click);
            // 
            // DateAndEngineTypeSliceButton
            // 
            this.DateAndEngineTypeSliceButton.Label = "Срез по типу двигателя и дате регистрации";
            this.DateAndEngineTypeSliceButton.Name = "DateAndEngineTypeSliceButton";
            this.DateAndEngineTypeSliceButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.DateAndEngineTypeSliceButton_Click);
            // 
            // VehicleAgeSliceButton
            // 
            this.VehicleAgeSliceButton.Label = "Срез по возрасту автомобиля и месту проживания владельца";
            this.VehicleAgeSliceButton.Name = "VehicleAgeSliceButton";
            this.VehicleAgeSliceButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.VehicleAgeSliceButton_Click);
            // 
            // Ribbon
            // 
            this.Name = "Ribbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tab1);
            this.Tabs.Add(this.VehicleRegLoban);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.VehicleRegLoban.ResumeLayout(false);
            this.VehicleRegLoban.PerformLayout();
            this.DataGenerationGroup.ResumeLayout(false);
            this.DataGenerationGroup.PerformLayout();
            this.DataAdditionGroup.ResumeLayout(false);
            this.DataAdditionGroup.PerformLayout();
            this.SqlQueryGroup.ResumeLayout(false);
            this.SqlQueryGroup.PerformLayout();
            this.GraphGroup.ResumeLayout(false);
            this.GraphGroup.PerformLayout();
            this.SGroup.ResumeLayout(false);
            this.SGroup.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonTab VehicleRegLoban;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup DataGenerationGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton GenerateDataButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup DataAdditionGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddColorButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddVehicleCategoryButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddRegionButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator3;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddModelButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddDistrictButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton LocalityButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddVehicleTypeButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddEngineTypeButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddBrandButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup SqlQueryGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton ExecuteQueryButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup GraphGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup SGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddRegistrationFactButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton ShowChartButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton DateSliceButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton DateAndEngineTypeSliceButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton VehicleAgeSliceButton;
    }

    partial class ThisRibbonCollection
    {
        internal Ribbon Ribbon
        {
            get { return this.GetRibbon<Ribbon>(); }
        }
    }
}
