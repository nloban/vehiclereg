﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Tools.Ribbon;
using Services;
using Services.Contracts;
using VehicleRegistrationExcelAddIn.Windows;
using VehicleRegistrationExcelAddIn.Windows.Addition;

namespace VehicleRegistrationExcelAddIn
{
    public partial class Ribbon
    {
        private readonly IRegistrationFactService registrationFactService = new RegistrationFactService();

        private void Ribbon_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void GenerateDataButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new DataGenerationForm();
            form.Show();
        }

        private void AddRegionButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new RegionAdditionForm();
            form.Show();
        }

        private void ExecuteQueryButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new QueryExecutionForm();
            form.Show();
        }

        private void AddColorButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new ColorAdditionForm();
            form.Show();
        }

        private void AddVehicleCategoryButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new VehicleCategoryAdditionForm();
            form.Show();
        }

        private void AddVehicleTypeButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new VehicleTypeAdditionForm();
            form.Show();
        }

        private void AddDistrictButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new DistrictAdditionForm();
            form.Show();
        }

        private void LocalityButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new LocalityAdditionForm();
            form.Show();
        }

        private void AddBrandButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new BrandAdditionForm();
            form.Show();
        }

        private void AddModelButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new ModelAdditionForm();
            form.Show();
        }

        private void AddEngineTypeButton_Click(object sender, RibbonControlEventArgs e)
        {
            var form = new EngineTypeAdditionForm();
            form.Show();
        }

        private void AddRegistrationFactButton_Click(object sender, RibbonControlEventArgs e)
        {
            new RegistrationFactAdditionForm().Show();
        }

        private void ShowChartButton_Click(object sender, RibbonControlEventArgs e)
        {
            new GetYearForChartForm().Show();
        }

        private void DateSliceButton_Click(object sender, RibbonControlEventArgs e)
        {
            new DateSliceForm().Show();
        }

        private void DateAndEngineTypeSliceButton_Click(object sender, RibbonControlEventArgs e)
        {
            new DateOfRegistrationAndEngineTypeSliceForm().Show();
        }

        private void VehicleAgeSliceButton_Click(object sender, RibbonControlEventArgs e)
        {
            var facts = registrationFactService.GetFactsByCarAgeAndRegions();
            ShowSlice(facts);
        }

        private void ShowSlice(Dictionary<String, List<RegistrationFact>> dictionary)
        {
            Globals.ThisAddIn.Application.Cells.Clear();
            InitializeHeaders(dictionary);
            InitializeData(dictionary);
        }

        private void InitializeData(Dictionary<String, List<RegistrationFact>> dictionary)
        {
            int width = dictionary.Count;
            int height = 3;
            int i = 2;
            int ageFromZeroToFive = 0;
            int ageFromFiveToTen = 0;
            int ageTenAndMore = 0;

            foreach (var item in dictionary)
            {
                var beforeFive = item.Value.Where(fact => ((fact.DateOfRegistrationId - fact.DateOfProduceId).Days / 365) < 5).Count();
                ageFromZeroToFive += beforeFive;
                Globals.ThisAddIn.Application.Cells[2, i] = beforeFive;

                var fromFiveToTen = item.Value.Where(fact => ((fact.DateOfRegistrationId - fact.DateOfProduceId).Days / 365) >= 5 && ((fact.DateOfRegistrationId - fact.DateOfProduceId).Days / 365) <= 10).Count();
                ageFromFiveToTen += fromFiveToTen;
                Globals.ThisAddIn.Application.Cells[3, i] = fromFiveToTen;

                var afterTen = item.Value.Where(fact => ((fact.DateOfRegistrationId - fact.DateOfProduceId).Days / 365) > 10).Count();
                ageTenAndMore += afterTen;
                Globals.ThisAddIn.Application.Cells[4, i] = afterTen;
                Globals.ThisAddIn.Application.Cells[5, i] = item.Value.Count;
                Globals.ThisAddIn.Application.Cells[5, i].Interior.Color = System.Drawing.Color.LightCoral.ToArgb();
                i++;
            }

            Globals.ThisAddIn.Application.Cells[2, i] = ageFromZeroToFive;
            Globals.ThisAddIn.Application.Cells[3, i] = ageFromFiveToTen;
            Globals.ThisAddIn.Application.Cells[4, i] = ageTenAndMore;
            Globals.ThisAddIn.Application.Range[GetExcelColumnName(i, 2), GetExcelColumnName(i, 4)].Cells.Interior.Color = System.Drawing.Color.DarkTurquoise.ToArgb();

            Globals.ThisAddIn.Application.Cells[5, i] = dictionary.Select(fact => fact.Value.Count).Sum();
            Globals.ThisAddIn.Application.Cells[5, i].Interior.Color = System.Drawing.Color.OrangeRed.ToArgb();

            Globals.ThisAddIn.Application.Range[GetExcelColumnName(1, 1), GetExcelColumnName(i, 5)].Columns.AutoFit();

        }

        private void InitializeHeaders(Dictionary<String, List<RegistrationFact>> dictionary)
        {
            int i = 2;
            foreach (var item in dictionary)
            {
                Globals.ThisAddIn.Application.Cells[1, i] = item.Key;
                Globals.ThisAddIn.Application.Cells[1, i].Interior.Color = System.Drawing.Color.Orange.ToArgb();
                i++;
            }

            Globals.ThisAddIn.Application.Cells[1, i] = "Всего";
            Globals.ThisAddIn.Application.Cells[1, i].Interior.Color = System.Drawing.Color.MediumTurquoise.ToArgb();

            Globals.ThisAddIn.Application.Cells[2, 1] = "До 5-ти лет";
            Globals.ThisAddIn.Application.Cells[3, 1] = "5-10 лет";
            Globals.ThisAddIn.Application.Cells[4, 1] = "Больше 10 лет";
            Globals.ThisAddIn.Application.Range[GetExcelColumnName(1, 2), GetExcelColumnName(1, 4)].Cells.Interior.Color = System.Drawing.Color.LimeGreen.ToArgb();
            
            Globals.ThisAddIn.Application.Cells[5, 1] = "Всего";
            Globals.ThisAddIn.Application.Range[GetExcelColumnName(1, 5), GetExcelColumnName(1, 5)].Cells.Interior.Color = System.Drawing.Color.Orchid.ToArgb();

        }

        private string GetExcelColumnName(int columnNumber, int rowNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return String.Format("{0}{1}", columnName, rowNumber);
        }
    }
}
