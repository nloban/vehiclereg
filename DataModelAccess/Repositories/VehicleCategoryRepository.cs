﻿using DataModelAccess.Contracts;
using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Repositories
{
    public class VehicleCategoryRepository : Repository<VehicleCategory>, IVehicleCategoryRepository
    {
    }
}
