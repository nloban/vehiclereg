﻿using DataModelAccess.Contracts;
using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Repositories
{
    public class RegionRepository : Repository<Region>, IRegionRepository
    {
    }
}
