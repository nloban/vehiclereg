﻿using DataModelAccess.Contracts;
using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Repositories
{
    public class RegistrationFactRepository : Repository<RegistrationFact>, IRegistrationFactRepository
    {
    }
}
