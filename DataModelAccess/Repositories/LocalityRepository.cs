﻿using DataModelAccess.Contracts;
using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Repositories
{
    public class LocalityRepository : Repository<Locality>, ILocalityRepository
    {
    }
}
