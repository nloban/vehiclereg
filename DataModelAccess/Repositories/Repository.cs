﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DataModelAccess.Contracts;
using Telerik.OpenAccess;
using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Repositories
{
    public class Repository<T> : IDisposable, IRepository<T> where T : class
    {
        protected VehicleRegistrationModel Context
        {
            get
            {
                return VehicleRegistrationModel.Current;
            }
            private set
            {
                if (value == null) throw new ArgumentNullException("value");
                Context = value;
            }
        }
        
        public virtual List<T> GetAll()
        {
            return Context.GetAll<T>().ToList();
        }

        public virtual List<T> GetBy(Expression<Func<T, bool>> predicate)
        {
            return Context.GetAll<T>().Where(predicate).ToList();
        }

        public virtual T Get(object id)
        {
            ObjectKey objectKey = new ObjectKey(typeof(T).Name, id);
            T entity = this.Context.GetObjectByKey(objectKey) as T;

            return entity;
        }

        public virtual void Add(T order)
        {
            Context.Add(order);
            SaveChanges();
        }

        public virtual void Add(List<T> orders)
        {
            foreach (var order in orders)
            {
                Context.Add(order);
            }
            SaveChanges();
        }

        public virtual void Remove(T order)
        {
            this.Context.Delete(order);
            SaveChanges();
        }

        public virtual void SaveChanges()
        {
            Context.SaveChanges();
        }

        public void Dispose()
        {
            this.Context = null;
        }
    }
}
