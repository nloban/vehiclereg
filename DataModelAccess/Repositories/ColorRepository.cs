﻿using DataModelAccess.Contracts;
using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Repositories
{
    public class ColorRepository : Repository<Color>, IColorRepository
    {
    }
}
