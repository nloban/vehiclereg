﻿using DataModelAccess.Contracts;
using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Repositories
{
    public class BrandRepository : Repository<Brand>, IBrandRepository
    {
    }
}
