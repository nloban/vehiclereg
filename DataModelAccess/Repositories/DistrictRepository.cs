﻿using DataModelAccess.Contracts;
using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Repositories
{
    public class DistrictRepository : Repository<District>, IDistrictRepository
    {
    }
}
