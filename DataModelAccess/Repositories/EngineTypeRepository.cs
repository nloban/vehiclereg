﻿using DataModelAccess.Contracts;
using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Repositories
{
    public class EngineTypeRepository : Repository<EngineType>, IEngineTypeRepository
    {
    }
}
