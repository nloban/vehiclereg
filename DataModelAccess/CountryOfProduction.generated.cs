using System.ComponentModel.DataAnnotations;
#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the ClassGenerator.ttinclude code generation file.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Common;
using System.Collections.Generic;
using Telerik.OpenAccess;
using Telerik.OpenAccess.Metadata;
using Telerik.OpenAccess.Data.Common;
using Telerik.OpenAccess.Metadata.Fluent;
using Telerik.OpenAccess.Metadata.Fluent.Advanced;
using VehicleRegistrationExcelAddIn;

namespace VehicleRegistrationExcelAddIn	
{
	public partial class CountryOfProduction
	{
		private string _countryId;


        [Display(Name = "Страна-производитель")]
		public virtual string CountryId
		{
			get
			{
				return this._countryId;
			}
			set
			{
				this._countryId = value;
			}
		}
		
		private IList<Brand> brands = new List<Brand>();
		public virtual IList<Brand> Brands
		{
			get
			{
				return this.brands;
			}
		}
		
	}
}
#pragma warning restore 1591
