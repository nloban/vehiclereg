﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Contracts
{
    public interface IDateDimensionRepository : IRepository<DateDimension>
    {
    }
}
