﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Contracts
{
    public interface IRepository<T> where T : class
    {
        List<T> GetAll();
        List<T> GetBy(Expression<Func<T, bool>> predicate);
        T Get(object id);
        void Add(T entity);
        void Add(List<T> orders);
        void Remove(T entity);
        void SaveChanges();
    }
}
