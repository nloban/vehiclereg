﻿using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Contracts
{
    public interface IEngineTypeRepository : IRepository<EngineType>
    {
    }
}
