﻿using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Contracts
{
    public interface IVehicleCategoryRepository : IRepository<VehicleCategory>
    {
    }
}
