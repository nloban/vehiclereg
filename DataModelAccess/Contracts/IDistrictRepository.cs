﻿using VehicleRegistrationExcelAddIn;

namespace DataModelAccess.Contracts
{
    public interface IDistrictRepository : IRepository<District>
    {
    }
}
