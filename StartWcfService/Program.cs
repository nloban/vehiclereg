﻿using System;
using System.ServiceModel;
using VehicleRegistrationWcfServiceLibrary;

namespace StartWcfService
{
    public class Program
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(ExecuteQueryService));

            host.Open();

            Console.WriteLine("Service is up and running");

            Console.ReadLine();

            host.Close();
        }
    }
}
